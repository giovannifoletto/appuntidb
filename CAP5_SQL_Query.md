## SQL: Data query Language

Vedremo come si elabora una `query` e come questa si interfacci con l’algebra relazionale.

Generalmente una query SQL ha 3 clausole principali, necessarie al funzionamento:

```sql
select	-- indica i dati da prendere/selezionare
from 	-- indica da che tabella/e prendere i dati
where	-- indica come selezionare i dati
```

Risultato è una relazione con gli attributi messi in input.



$\pi_{name, income} (\sigma _{age< 30} (person))$ Questa query in algebra, si traduce in:

```sql
select person.name, person.income
from person
where person.age < 30
```

Questa operazione è che prende una tabella e inizia a scansionare le righe, poi la where indica le clausole per tenere o buttare una riga.



Notiamo che l’attributo viene inserito con il nome della tabella prima : `tabelName.objectName`. Se non ci sono ambiguità allora si può evitare tutto. 

Posso anche rinominare degli oggetti per l’interrogazione:

```sql
select p.name as name, p.income as income
from person as p -- rename persone as p
where p.age < 30
```

Inoltre il `p.name as name` è un modo per cambiare il nome delle colonne.



Il risultato di operazione per SQL non rimuove i duplicati, perché è troppo costosa dal punto di vista computazionale. Per avere un elenco di elementi unici, allora devo:

```sql
select surname, city from employees -- not remove duplicates

select distinct surname, city from employees -- removes duplicates
```



Tutti altri operatori sono poi definiti come un normale linguaggio di programmazione:

```sql
select name, age, income
	income as repeatedIncome -- sto aggiungendo una 4ta tabella con la copia dello stipendio
from person
where income >= 20 and imcome <= 30

```

Aggiungere una 4ta colonna in questo modo è molto utile se si vuole modificare i dati in modo programmatico e mantenere il dato originale.



Nel SQL si può avitare di dover scrivere tutte le righe se la selection vuole mantenere tutto:

```sql
select *
from person
where person.age < 30
```

Se non si mette la `where` si esegue solo una proiezione, ovvero quando restituisco tutto.

La relazione più facile è quella che restituisce tutto dal database.



## Operazioni sugli attributi che restituiamo

```sql
select income/2 as semesterIncome
from person
where name = 'Luigi'
```

Questa operazione divide `income`, dalla tabella `person`, quando `name == 'Luigi'`:

| person.name | person.age | ...  | income    | semesterIncome          |
| ----------- | ---------- | ---- | --------- | ----------------------- |
|             |            |      | stipendio | stipendio/2 (annuale/2) |



Per ottenere il risultato degli attributi senza ripetizioni, utilizzo `distinct`:

```sql
select distinct surname, city
from employees
```



### Complex conditio in the “where” clause

La clausola `where` è necessaria per svolgere le selezioni, ovvero la selezione delle righe della tabella. In questo modo si svolge un operazione di questo tipo:

```sql
select name, income
from employees
where name = 'Luigi';
```

Dove l’eguaglianza è il simbolo `=` (mentre invece solitamente nei linguaggi di programmazione è `==`).

Ci possono essere anche delle clausole di questo tipo più complicate, che sfruttano una serie di operatori matematici o logici:

```sql
select *
from person
where income > 25 and (age < 30 or age > 60);
```

Di fatto questa clausola richiede solo delle espressioni che, operate su ogni riga della tabella, possono restituire un valore booleano (`true` o `false`).



Un altro costruttore che l’algebra relazionale non possiede è il costrutto `like`, da utilizzare nella clausola `where`. Questa keyword è utile per trovare delle forme di stringhe, di cui conosco solo alcune parti o una struttura particolare:

```sql
select *
from person
where name like 'A_d%' -- descrive un pattern
```

In questo caso, il pattern esegue un match positivo nel caso di una stringa così costruita:

`A_d%`: A maiuscola prima, seconda lettera a caso, la 3za `d` minuscola e poi qualsiasi cosa.

Chiaramente gli `_` e i `%` possono essere utilizzati all’interno della stringa quante volte è necessario, dove nel primo caso (ovvero degli `_`), si può avere il numero di lettere identificato dal numero di `_`.



## `NULL` values

Visto che la logica relazionale permette la creazione di questi valori, studiamo il comportamento nel caso un valore/attributo è uguale a `null`.

```sql
select *from employeeswhere age > 40 or age is null -- se non faccio questa operazione, se trovo una riga `null` allora, il risultato in questa relazione è un valore non noto, ovvero l'operazione risulta sconosciuta
```

Siccome questo valore ritorna un valore booleano, in questo caso (`true`, `false`, `null`). In questo caso di fatto, ritorna il valore solo delle risposte vere, non anche quelle per cui la risposta è nulla.

Da notare l’utilizzo di `is`, che è diverso da `age = null`. Questa operazione può portare a effetti non conosciuti se si utilizza il `=`, siccome `age == null` se ` age` è `null`, allora `null == null`, operazione che non può essere svolta.

Inoltre l’`=` confronta due oggetti appartenenti allo stesso dominio, e quindi non si può fare questa operazione con il `null`.



## Le operazioni di Join

Questa operazione si fa specificando più tabelle nella clausola from.

Abbiamo due relazioni $R_1(A_1, A_2)$ e $R_2(A_3, A_4)$.

SQL per questa operazione è l’implementazione del prodotto cartesiano, quindi crea tutte le combinazioni:

```sql
select R1.A1, R2.A4
from R1, R2 
where R1.A2 = R2.A3
```

tenicamente questa è l’operazione $\pi_{A_1, A_2}(R_1\Join_{A_2 : A_3} R_2)$.

Di fatto questa scrittura imposta il prodotto cartesiano con tutti gli elementi, ma allo stesso modo controlla che l’uguaglianza dei due valori in oggetto.

Questa scrittura è di base l’implementazione del prodotto cartesiano vero e proprio, ma spesso questo non è l’ordine delle operazioni svolte dal DBMS, siccome questo modello ha un costo non indifferente.

Questo avviene anche perché le query SQL sono **dichiarative**, ovvero indicano solo l’output che si vuole ottenere, non il modo che serve per ottenerlo. Questo solitamente viene implementato dal DBMS nel miglior modo possibile, per ottenere l’interrogazione ottimizzata al meglio.

Spesso durante l’esecuzione di una join è utile svolvere una rinominazione precedente all’operazione della join.

```sql
select Y.A1, W.A4
from R1 as Y , R2 as W 
where Y.A2 = W.A3
```



##### Exercise 5

```sql
select c.name, c.income, p.income
from person p, isFather t, person 
cwhere p.name = t.father and t.child = c.name and c.income > p.income
```



Inoltre spesso è utile utilizzare la parola `distinct` all’interno delle `join`.

Inoltre abbiamo la necessità di svolgere la **join naturale**, ovvero dove si ha un parametro uguale in entrambe le tabelle, e utilizzare questo parametro come operatore di confronto per unire gli elementi:

```sql
select isMother.child, father, mother
from isMother, isFather
where isFather.child  = isMother.child
```



Come abbiamo notato, in questo metodo di viene a mescolare molte condizioni, alcune riferite alle join, altre riferite al filtro dei dati. Per rendere le condizioni SQL più leggibili, allora si opera una **explicit join** utilizzando la *keyword* `join` nel costrutto `from` (questa indicazione quindi è pura sintassi, non cambia nulla l’output dell’operazione):

```sql
select mother, isFather.child, father
from isMother join isFather on isFather.child = isMother.child 
-- prendi isMother è fai la join se isFather.child == isMother.child.
```

In questo modo poi nella `where` si possono aggiungere tutte le informazioni necessario al filtraggio dei dati, rendendo tutto il costrutto più leggibile.



Un’altra operazione molto utilizzata nell’algebra relazionale è la **join naturale**, ovvero che esegue il confronto con due tabelle dello stesso nome. Questa operazione si può ottenere nel modo presentato sopra, oppure, più semplicemente con la keyword perdisposta:

```sql
select isFather.child, father, mother
from isMother natural join isFather

-- espressione che equivale a

select isFather.child, father, mother
from isMother join isFather on isFather.child = isMother.child
```

Questo comunque, rimane un metodo utilizzato meno degli altri.



Ci sono due altri tipi di join che sono utilizzati nel linguaggio SQL, ma che non appartengono completamente all’algebra relazione. Questi comandi permettono di avere delle operazioni più veloci su alcuni concetti, con un interrogazione più ordinata e leggera: **left outer join** oppure **left join** e **right outer join** oppure **right join**.

Operando la select su queste tabelle si otterrebbe una terza tabella in cui è avvenuta l’operazione richiesta. Di fatto se si ha una persona che non è madre di nessuno, allora questa colonna sulla sinistra non effettuerà nessun match, e quindi verrà lasciata fuori dalla tabella finale. Se invece volessi questo dato, quando possibile (se no si restituisce `null`):

```sql
select isFather.child, father, mother
from isFather left join isMother on isFather.child = isMother.child
```

Di fatto quindi questo significa che la tabella di destra, se ha valori nella join viene restituita con tutti i suoi valori, se no viene restituita ugualmente, ma con tutti gli attributi a lei collegati settati a `null`. (Di fatto quindi questa interrogazione restituirà tutti i `isFather`, anche se non hanno nessun match nell’operazione di join)

Uguale vale con l’operazione di `right outer join`, semplicemente che si ha come tabella di partenza quella di sinistra. (Questa interrogazione quindi restituirà tutti i `isMother`, nonostante non abbiano nessun valore che effettui un match all’interno della relazione).



### Altri costrutti utili

Se si vuole aggiungere un ordine agli elementi restituiti si può utilizzare il costrutto `order by`:

```sql
select name, income
from person
where age < 30
order by name
```

In questo modo, indicando l’attributo sul quale si vuole effettuare un ordinamento, allora si opera un ordinamento in base al tipo che contiene (quindi con le stringhe, con i numeri e con altri tipi possibilit, tipo le date). 

Di base si effettua un ordine crescente, mentre invece se si vuole un ordinamento decrescende basta aggiungere la keyword `desc`:

```sql
select name, income
from person
where age < 30
order by name desc
```



Si può ottenere un numero finito di risultati, ovvero si scelgono solo le righe che sono comprese in quel determinato numero:

```sql
select name, income
from person
where age < 30
limit 10 -- prende solo i primi risultati di questa interrogazione
```

Solitamente questa *keyword* è molto utilizzata all’`order by`.



### Operazioni di aggregazione

Questi operatori sono delle ‘funzioni’, che permettono di svolgere alcune operazioni sulla base di dati. Queste funzioni possono essere **count, min, max, avarage, total** e generalmente utilizzano una sintassi di questo tipo `function ([distinct] Expression)`.

Alcuni esempi:

```sql
-- questa interrogazione ritorna il conto del numero di persone con nome Franco.
select count(*) as numChilderFranco
from isFather
where father = 'Franco'
```

In questa funzione si contano tutte le righe della tabella, quindi non c’è nessun problema, ma ad esempio se si opera:

```sql
select count(income)
from person
```

allora si ottiene una selezione sull’attributo `income`. In questo caso si ottiene che il DBMS ignora gli attributi che contengono il `null`. In questo modo quindi si potranno trovare meno valori di `income` rispetto alle righe della persona.

Inoltre se si inserisce la *keyword* `distinct` allora si ottiene un conteggio riferito soltanto agli elementi distinti dell’interrogazione (nella query sotto quindi ritornerà quanti stipendi unici ci sono nella tabella):

```sql
select count(distinct income)
from person
```

(Però  `distinct` ha sempre necessità di operare su un attributo)



Ci sono alcune operazioni puramente matematiche, che hanno senso solo quando operano su valori numerici oppure date/tempo. Queste operazioni sono `sum` (somma), `avg` (media), `max` (valore massimo), `min` (valore minimo), e vengono utilizzate allo stesso modo di tutte le altre:

```sql
select avg(income)
from person join isFather on name = child
where father = 'Father'
```

Questa interrogazione torna la media dei valori dove il padre dei bambini è chiamato `Father`.

Anche questi operatori ignorano ogni valore definito `null`.

Di fatto tutte questi operatori possono essere accoppiati, nel costrutto di `select`, per avere delle colonne con all’interno dei valori particolari. In questo modo si possono avere delle colonne che contegono sempre la funzione sopra, applicata a tutti i valori che si vogliono restituire insieme a questo:

```sql
select avg(income),person.name
from person join isFather on name = child
where father = 'Father'
```



### Operazione di aggregazione

Questo operatore è sempre rappresentato dalla *keyword* `group by`. Questa keyword permette di partizionare la tabella in alcuni gruppi. Dopodichè si possono operare delle funzioni (precedentemente spiegate) per contare i vare gruppi. 

Ad esempio si può utilizzare per trovare il valore di tutte le volte che un nome particolare è padre, attraverso questa query:

```sql
select father, count(*) as NumChildren
from isFather
group by father
```

![image-20220119234642344](image/image-20220119234642344.png)



Spesso insieme a questa clausola, si utilizza le `having`. Di fatto questa indicazione è molto simile alla `where`, con l’unica differenza che questa *keyword* opera sul gruppo.  

Ad esempio:

```sql
select father, avg(c.income)
from person c join isFather on child = name
group by father
having avg(c.income) > 25
```

Se non opero questa clausola con il costrutto `group by` allora `having` opera sempre su un aggregatore (ovvero una delle funzioni) e la richiede su tutta la tabella. Si può utilizzare questo modello per avere un filtro su delle operazioni tra le persone nella tabella, come ad esempio richiedere le persone che hanno lo stipendio maggiore. 

Un esempio di una query con un `having` può essere:

```sql
select father, avg(c.income)
from person c join isFather on child = name
where c.age < 30
group by father
having avg(c.income) > 20
```



## Riassunto:

una query quindi può avere tutti questi costrutti, possibilmente:

```sql
select <colums>
from <tables>
[where <condition>]
[group by <attributes>]
[having <aggregate conditions>]
[order by <attribute>]
[limit <number>]
-- dove tutte le informazioni contenute tra `[]` sono opzionali per il funzionamento della query, ma servono a ottenere dei risultati diversi
```



### Costrutto di unione, intersezione e differenza 

Il costrutto per l’operazione di *unione* tra due tabelle si ottiene con la *keyword* `union`.  Questo costrutto si utilizza in questo modo:

```sql
select *
from --

union

select *
from *
```

Questo operatore deve avere degli attributi che ritornano due tabelle *union-compatible* ovvero con lo stesso numero di colonne, con lo stesso ordine, contenenti lo stesso tipo di dati. 

Postgres è capace di unire automaticamente due colonne se trova la stessa intestazione in entrambe, a patto che contengano lo stesso tipo di dati.

Inoltre in SQL si può operare `union all`, che permette di ottenere una richiesta più ottimizzata, siccome restituisce anche i valori duplicati, senza operare il controllo che fatti invece dalla `union`.

Per cambiare l’ordine degli attributi, oppure selezionarne alcuni di una o dell’altra è necessario operare sulle due “sottoquery” operate dall’union:

```sql
select father as parent, child
from isFather

union

select mother as parent, child
from isMother
```



L’operazione di differenza si opera e funziona nello stesso modo:

```sql
select --
from --

except

select --
from --
```

Ed allo stesso modo:

* `except` opera l’eliminazione delle tuple duplicate
* `expect all` non opera questa funzione (rendendo la query più ottimizzata)

 

Infine l’operazione di intersezione, si può ottenere con:

```sql
select --
from --

intersect

select --
from --
```

che in realtà è equivalente a:

```sql
select distinct i.name
from employee i, employee j
where i.name = j.surname
```

Inoltre anche questa clausola presenta le due varianti `intersect` e `intersect all`.



### Nested Queries

Si possono raggruppare delle query, e utilizzare l’output di queste per operare altre funzioni. In questo modo si possono spezzare dei problemi più grandi in piccole operazioni più facili da svolgere.

Un esempio di questa operazione può essere `Il nome e lo stipendio delle persone che hanno come papà franco`, e si ottiene:

```sql
select name, income
from person
where name in (
	select father
    from isFather
    where child = 'Franco'
)
```

Da notare che l’utilizzo di `in` in questa query. Infatti non si può utilizzare l’`=`, perché si presume che la sottoquery restituisca più di una riga, e quindi di operare un confronto su un gruppo di elementi.

Le nested query adesso offrono un sacco di potenza, siccome dividono molti problemi in tantissimi problemi molto più piccoli. Di fatto quindi esistono delle keyword.

Ad esempio, il problema di prima si poteva risolvere anche con la *keyword* `any` posta davanti al gruppo creato con la sottoquery:

```sql
select name, income
from person
where name = any(
	select father
    from isFather
    where child = 'Franco'
)
```

In questo caso si ottiene un confronto operato con ogni elemento restituito dalla sottoquery (esattamente come avveniva con il `in`). La differenza di questa *keyword* è che può essere utilizzata con qualsiasi operatore logico, ad esempio `< any` oppure `> any`, o qualsiasi altro operatore funzionante in questo DBMS.

Altri simboli che possono essere molto utili è la *keyword* `not`, che si può prosporre a qualsiasi operazione e ottenere il contrario. Nel caso si operasse `not in`, lo stesso risultato si può ottenere con `<> any` (in SQL `<>` è il simbolo per diverso).

Un altro indicatore che si può utilizzare in una sottoquery è `exist`. Questa indicazione è necesaria per indicare per effettuare una selezione in un sottogruppo. In questo modo si può ritornare solo se un elemento è presente anche nella sottoquery.



Le sottoquery possono essere anche molto ramificate e sfruttare a loro volta altre sottoquery, ad esempio:

```sql
select name, income
from person
where name in (
	select father
    from isFather
    where child in (
    	select name
        from person
        where income > 20 
    )
)
```



Di fatto l’utilizzo di sottoquery rende delle interrogazioni molto più facili da costruire, da riutilizzare e da leggere. Il lato negativo di questa operazione però viene con la perdita di efficienza, infatti li DBMS non riesce a ottimizzare una serie di sottoquery di questo tipo.



## PostgreSQL e SQL

PostgreSQL è un DBMS server open source e multi-utente. In questo modo si possono avere degli accessi autonomi, da più architetture e diversi client possono collegrsi e fare richieste.

Ci sono alcuni comandi proprietari di postgreSQL:

* `psql -h localhost -U postgres`: si accede al database sul localhost (host) con l’user postgres
* Quando si accede si ottiene una nuova riga di comando, con `postgres#` all’inizio
* con `\c` si può cambiare database all’interno del server
* con `\d` si ottiene l’elenco delle tabelle dello schema `public`
  * Un database in postgres può contenere tantissimi schemi, ovvero delle tabelle relazionali che partecipano tutte assieme alla collezione dei dati del database.
  * in questo modo si possono anche dividere gli schemi in diversi namespace
* con  `\d <nometabella>` si ottiene lo schema della tabella
* si possono lanciare anche i comandi SQL, quindi tutti quelli visti precedentemente.
* `CREATE DATABASE <nomeNewDB>` per creare una nuova base dati
  * una volta operata questo comando, si possono inserire le tabelle in questo database e operare con tutti i comandi SQL tra le tabelle
* con `psql` si può anche specificare `-f nomefile.sql`, che permette di eseguire un file da riga di comando che contiene più di un comando SQL. IN questo modo si possono lanciare una serie di comandi insieme con un unico comando.

Quando davanti alla query si indica `EXPLAIN` allora si crea un grafo che spiega l’albero di esecuzione della query. In questo modo si può vedere le operazioni tra tabelle della base di dati, e quindi come ha operato il DBMS.

Si possono creare delle *view* all’interno del database:

```sql
CREATE [TEMP] VIEW <name-query> AS (
	select	-- select value
    from 	-- from value
    		-- other values
)
```

In questo modo si può interagire con questa nuova `view` come fosse una tabella a se stante, interrogandola oppure utilizzandola come base di partenza per altre interrogazioni.

Il parametro `TEMP` è facoltativo e serve a indicare al DBMS che non è necessario mantenere costante la view perché è temporanea.

Una view si può tranquillamente buttare con `DROP  [TEMP] VIEW <name-view>`.

Di fatto questa view è una tabella a tutti gli effetti, però gli elementi che contiene rimangono invariati, nonostante si cambino i dati nella base originale. Per aggiornare i dati che contiene è necessario rilanciare la creazione di questa view.



## Interrogazione e `NULL`

Il comportamento delle funzioni sulle tabelle, in funzione all’esistenza del `NULL` è molto variabile:

```sql
select count(*) from R; -- in questa query restituisce il numero di tutte le righe della tabelle (restituendo il numero di tutte le righe, nonostante quest contengano sempre solo null)
select count(<colonn>) from R; -- in questa query invece si conta una colonna che può contenere dei valori nulli, in questo modo se li trova li ignora.
```

Di fatto la stessa tabella che contiene tutti valori `null` come `R` dell’esempio ottiene due valori diversi in due query che dovrebbero risultare due valori uguali.

Tutte le funzioni di aggregazione lavorano ignorando tutti i valori `null` quando si opera su delle colonne specifiche.

Differentemente invece, si ottiene un comportamento diverso con `group by` che tende a raggruppare tutti i valori `null` insieme. Questo di solito è dovuto al pensiero che si tenda a perdere meno dati possibili in ogni interrogazione.

Il costrutto `join` invece quando incontra un valore `null` scarta l’intera tupla. Di fatto questo viene fatto per avere un comportamento simile al comportamento del comando `where`. Allo stesso modo, operando un comando di `left/right join` allora se si trova un valore `null` in una delle due tabelle (dichiarata come valore sempre da prendere) allora il DBMS mantiene questo valore anche se `null`. (Questo è comportamento opposto rispetto alla normale `join`).



Si possono per questi motivi avere dei comportamenti particolari e avere la necessità di inserire un valore di default in casi dubbi. La funzione standard che permette di operare questa funzione è `COALESCE(<valore1>, <valore2>, ...)`. Questa funzione riceve una sequenza di valori e restituisce il primo di questi che non è `null`. 

Questa funzione serve quando si lavora con delle tabelle che possono restituire dei valori `null`. Ad esempio in una tabelle in cui bisogna restituire la lingua di un film e il numero di film in quella lingua si potrà operare in questo modo

```sql
select language_id, coalesce(sum(*), 0)
from film
group by language_id;
```

In questo modo si avrà sempre la capacità di settare a `0` il valore che viene restituito nullo.

Un costrutto particolare del DBMS postgreSQL si può avere nel costrutto `having`. In questa clausola si possono dichiarare che la mia caratteristica sia valida logicamente per tutti gli elementi del sottogruppo per un determinato operatore logico:

```sql
select act.actor_id, count(fa.film_id)
from actor act left join film_actor fa on act.actor_id = fa.actor_id join film f on fa.film_id = f.film_id
group by act.actor_id
having every(coalesce(f.lenght, 0)> 170);
-- in questo modello si pone che si prenda il valore della lunghezza del film oppure il valore 0
```



 



