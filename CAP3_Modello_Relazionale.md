# Capitolo 3: Modello Relazionale

Il database relazionale è un database che sfrutta il modello relazionale, ovvero una raccolta di relazioni.

Le relazione è un nome strano per indicare una tabella di dati, ma chiamato in questo modo diverso perché:

* la tabella sono i dati, e può essere anche chiamata **istanza**

  * insieme di dati fatto da righe (cardinalità della tabella o istanza) e da colonne (il cui numero si chiama grado o arità della relazione)
  * **schema**: descrizione in un determinato linguaggio che indica:
    * il nome della tabella
    * il nome di ciascun campo
    * indica il tipo di ogni attributo

* la relazione descrive i dati che la tabella contiene, le relazioni fra le tabelle sono tutte queste informazioni chiamate anche **relazioni**

* Un esempio di una relazione potrebbe essere:

  ```sql
  E.G Students(sid: string, name: string, login: string, age: integer, gpa: real);
  ```

* La relazione può essere quindi di fatto rappresentata come un **insieme di righe** (ogni riga rappresenta un entità) senza un ordine preciso. Le righe si possono anche chiamare `tuple` (ovvero un insieme di elementi)

<img src="image/image-20211124113330979.png" alt="image-20211124113330979" style="zoom:50%;" />

* un vincolo che esiste nel modello matematico del database è che ogni tupla sia unica. Il problema su questa assunzione è che è molto difficile da mantenere, quindi spesso i DBMS rilassano questo vincolo
  * Di fatto quindi non si hanno `tuple` uguali (anche perché sarebbe inutile avere due dati uguali).
  * Le righe possono anche avere degli elementi uguali, l’importante è che le tuple siano uniche.



## SQL: Structured Query Language

Nel DBMS si ottiene la gestione delle relazioni attraverso il linguaggio `SQL`. Questo linguaggio è stato sviluppato da IBM nel 1970 per il suo primo sistema di immagazzinamento dati. Questo linguaggio è uno standard molto utile perché permette di utilizzare questo linguaggio su qualsiasi tipo di server (a parte qualche differenza proprietaria).

Il linguaggio SQL ha molte feature, e di solito si dividono in due grandi gruppi di comandi: **DDL (Data Definition Language)** e 



### SQL: DDL (Data Definition Language)

Questa sezione del linguaggio permette di modificare e creare lo schema logico del database, indicando tutti gli attributi e le loro variabili.

Ad esempio, per creare la relazione `Students`:

```sql
CREATE TABLE Students
(
	sid: CHAR(20),
    name: CHAR(20),
    login: CHAR(10),
    age: INTEGER,
    gpa: REAL
)
```

(Buona norma indicare i comandi in maiuscolo, ma funzionano anche in minuscolo. Si può fare anche tutto in riga).

Il tipo di dati di questi valori sono tantissimi, in questo corso varieranno su:

* `INTEGER`: interi
* `REAL`: numeri float
* `CHAR(<n>)`: stringa di caratteri con `n` valori (sempre fissi).
  * Questo significa che il DBMS se riceve un valore con numero di caratteri minore di `n`, riempe il resto con valore nullo. 
  * Questo di solito si utilizza per valori di cui conosco sempre la lunghezza (come ad esempio il codice fiscale)
  * Il DBMS ha comunque sempre bisogno di sapere la dimensione massima di una determinata stringa, oppure non saprebbe come ottimizzare le funzioni che deve operare.
  * Se si vogliono delle stringhe si può utilizzare il `VARCHAR(<n>)`. La differenza è che questa indicazione non riempirà il restante spazio con del valore nullo, cosa che si otterrebbe con `CHAR(<n>)`.
* è necessario il `;` per concludere il comando, oppure non viene eseguito

#### Eliminazione e Modifica dello schema e dell’istanza

Se si vuole **eliminare** la tabella si può utilizzare il comando `DROP Table`. Questa operazione elimina tutto il contenuto e *anche* lo schema. Quindi viene eliminata l’intera tabella, compreso il valore delle colonne.

Se invece si vuole **modificare lo schema** della tabella si può utilizzare il comando:

```sql
ALTER TABLE Table
	ADD COLUMN nameCol: type;
```

Se questa operazione viene eseguita su una tabella non vuota, il DBMS riempe tutti i valori appena aggiunti delle *tuple* preesistenti con il valore `null`. Molto pericoloso. Questo tipo di valore è pericoloso perché il `null` nei database relazionali significa *valore sconosciuto* (a differenza dei linguaggi di programmazione che invece hanno questo valore indicato come zero semplicemente). 

Il `null` per come è indicato non è passibile di nessuna richiesta. Ad esempio se si confronta questo valore con un valore numerico, non si sa se è maggiore, minore o uguale. Il problema è che questo valore può cambiare il comportamento del DBMS in base a che questo valore ritorna (di solito comportamento non costante).



#### Vincoli di Integrità

Per definire una relazione inoltre è necessario mantenere alcune condizioni tra e nelle relazioni che devono essere sempre vere. Questa operazione si ottiene con dei **vincoli d’integrità (ICs: Integrity Constraints)**. Questi vincoli sono delle condizioni che se non sono rispettate non rendono valida la tabella.

Il DBMS ha il compito di controllare che non esistano istanze (tabelle) che rompano questi vincoli.

I vincoli inseriti in questo schema si specificano guardando al diagramma ER. Questo significa che i vincoli sono la conseguenza del progetto del nostro database per essere quanto più vicino alla realtà, obbligando di fatto un comportamento definito in base ai comportamenti dei dati.

##### Vincolo di Chiave primaria

Il più semplice vincolo che si può utilizzare è il **vincolo di chiave primaria**. Questa chiave è il valore che utilizziamo per indicare il valore unico in un *entity set*. Questo valore da rendere unico è un insieme di attributi (più minimale possibile) che crea la **chiave candidata**. Questo valore è **candidata** perché abbiamo deciso che possa essere un valore univoco, ma può essere anche **primaria** se tra le candidate si decide una in particolare. Questo valore fornisce il vincolo di:

* non esistenza di due righe con chiave candidata uguale 
  * Esempio: l’`id` di due studenti
* di fatto può essere un insieme di chiavi candidate ogni sottoinsieme di valori presenti nella relazione che contiene la chiave principale (in questo caso `id` ).
  * Questi sottoinsieme viene chiamato *chiave*, ma non è minimale come invece serve per indicare la chiave *primaria*.

Questo vincolo è necessario per indicare la chiave primaria di una tabella, attraverso questo valore:

```sql
CREATE TABLE Enrolled(
	sid CHAR(20),
    cid CHAR(20),
    grade CHAR(2),
    PRIMARY KEY(sid, cid)
)
```

Questa tabella indica che non si possono avere lo stesso studente nello stesso corso con due voti diversi (quindi non va bene`id=1, C=1, g= 10; id=1, C=1, g=11`).

Di fatto la chiave primaria è unica, ma si può imporre che vengano valutate delle condizioni di unicità anche per delle chiavi candidate, allora si può utilizzare la parola chiave `UNIQUE`:

```sql
CREATE TABLE Enrolled(
	sid CHAR(20),
    cid CHAR(20),
    grade CHAR(2),
    PRIMARY KEY(sid),
    UNIQUE(cid, grade)
)
```

Questa tabella indica che, uno Studente può:

* partecipare a un solo corso e avere un solo voto (con la `PRIMARY KEY`, siccome lo `student_id` può essere associata a un solo corso)
* il costrutto `UNIQUE` indica che non si può avere nello stesso corso due studenti con lo stesso voto (non ha senso nella logica, ma funziona come costrutto)

Questo costrutto è importante perché permette di avere degli attributi con valori non nulli, allora utilizza questo costrutto per diversificare le tuple. Se invece questi valori permette a memorizzare il valore lo stesso.

Questo non avviene con `PRIMARY KEY()` che obbliga il valore a essere diverso da `null`.

Chiaramente la cosa si fa pericolosa, perché si potrebbero creare delle tuple uguali con un valore `null` che in realtà potrebbe essere uguale.

Chiaramente, come si vede dall’ultimo esempio, se il **vincolo d’integrità** è scorretto dal punto di vista logico, allora si sta impedendo al nostro DBMS di raccogliere dei valori che sarebbero accettabili. 



##### Vincolo di Chiave esterna

Il secondo tipo di vincolo è il **vincolo di chiave esterna** (`FOREIGN KEY`). Questo attributo indica che nello spazio del valore indicato non ci possono essere tutti i valori arbitrari possibili, ma si deve avere un dato preso da un’altra tabella.

Ad esempio: la tabella di partecipazione di un corso con il campo studenti, non potrà avere dei numeri di matricola casuali, ma solo dei valori di matricole presi dalla tabella degli studenti. 

Se questo vincolo viene eseguito allora si dice che la nostra base di dati ha la proprietà di **referential integrity (integrità referenziale)**. Questo significa che ogni riferimento non è nel vuoto o punta a un oggetto che non esiste (dangling references), ma indica sempre un valore presente nell’altra tabella.

```sql
CREATE TABLE Students(
	sid INTEGER, name VARCHAR(20), login VARCHAR(20), 
    age INTEGER, gpa INTEGER 
)
CREATE TABLE Enrolled(
	esid CHAR(20), cid CHAR(20), grade CHAR(2),
    PRIMARY KEY(esid, cid),
    FOREIGN KEY (esid) REFERENCES Students
)
```

Questo indica che ogni `Enrolled` contiene una matricola di uno studente proveniente dalla tabella  `Students`. 

Per enfatizzare il fatto che questa informazione è esterna si utilizza `esid` invece che `sid` (`e` di *external*). 

Inoltre si utilizza un vincolo di `FOREIGN KEY` che fa riferimento alla tabella `Students`. Ma il DBMS non ha necessità di sapere quale chiave di `Students` deve prendere, ma prende solamente la *chiave primaria* della tabella. 

La chiave primaria quindi è il *default* per la chiave primaria in queste relazioni.

 

##### Mantenimento dei vincoli di integrità

Se si cerca di forzare l’inserimento di un dato che non rispetta i vincoli, e il DBMS ha tantissime opzioni tra cui si può specificare:

1. a **cascata** eliminare tutte le righe che non sono legali, anche nella tabella da cui parte la richiesta con un valore esterno
2. non permettere completamente l’operazione dal principio
3. se elimino il valore, impostare la chiave primaria con un valore di default, oppure (se ci si vuole male) con il valore `null`.

Per specificare questo comportamento allora si utilizza l’operatore `ON DELETE`:

```sql
CREATE TABLE Enrolled(
	sid CHAR(20) DEFAULT '',
    cid CHAR(20) DEFAULT '',
    grade CHAR(2),
    PRIMARY KEY (sid, cid),
    FOREIGN KEY (sid),
    	REFERENCES Students
    		ON DELETE CASCATE
    		ON UPDATE SET DEFAULT
)
```

E questa operazione può essere:

* `ON DELETE NO ACTION`: **impedisce di farlo**
* `ON DELETE CASCADE`: **impedisce anche al `FOREIGN KEY`**
* `ON DELETE SET NULL/ SET DEFAULT`. Ne, caso del **default** è necessario definirlo vicino al valore: 

Di base se non si specifica nulla interpreta `NO ACTION` sia per `UPDATE` sia per `DELETE`. Questo perché è l’opzione più conservativa che permette di non fare danni e mantenere valido il database.



### DB Design: dall’ER allo schema relazionale

Il linguaggio DDL sono l’insieme degli attributi e dei valori di descrizione dell’entità.

Questa sezione rappresenta la creazione dello schema logico (passaggio dallo schema ER alla struttura del database).

Questo passaggio può essere fatto in questa modalità:

<img src="image/image-20211124173440443.png" alt="image-20211124173440443" style="zoom:50%;" />

che corrisponde al codice:

```sql
CREATE TABLE Employes
	(
    	ssn CHAR(11),
        name VARCHAR(20),
        lot INTEGER.
        PRIMARY KEY (ssn)
    );
```



Mappare le **relazioni** invece è più difficile. Partiamo dalla relazione molti a molto e non hanno vincoli da nessuna delle due parti:

<img src="image/image-20211124173556953.png" alt="image-20211124173556953" style="zoom:50%;" />

```sql
CREATE TABLE Employes 
(
	ssn CHAR(11),
    name VARCHAR(20),
    lot INTEGER.
	PRIMARY KEY (ssn)
);

CREATE TABLE Departments
(
	did INTEGER,
    dname VARCHAR(20),
    budget INTEGER,
    PRIMARY KEY(did)
);

CREATE TABLE Works_in
(
	ssn CHAR(11),
    did INTEGER,
    since DATE,
    PRIMARY KEY(ssn, id),
	FOREIGN KEY(ssn)
    	REFERENCES Employes,
    FOREIGN KEY (did)
    	REFERENCES Departments
);
```

Il DBMS impone la `PRIMARY KEY` anche nella tabella `Works_in` siccome la validazione non è espressamente definita e quindi si potrebbero avere delle tuple ripetute, nonostante si passasse da due tabelle con valori univoci.

Le **relazioni con vincoli di esistenza** diventano più impegnativi e lasciano spazio a multiple implementazioni:

<img src="image/image-20211124174739182.png" alt="image-20211124174739182" style="zoom:67%;" />

Questo significa che:

* un dipendente può gestire 0 o più dipartimenti
* un dipartimento può avere uno e uno solo gestore.

Il vincolo in questo caso è una tabella centrale come l’esempio precedente:

```sql
CREATE TABLE Managers(
	ssn CHAR(11),
    did INTEGER,
    since DATE, 
    PRIMARY KEY (did), -- Impedisco la duplicazione di dip.
    FOREIGN KEY (ssn)
    	REFERENCES Employees,
    FOREIGN KEY (did)
    	REFERENCES Departments
);
```

In questo modo:

*  se voglio aggiungere un valore di un dipartimento (che non è obbligatorio), allora basta indicare il `did` del determinato dipartimento per essere aggiunto. Se però decido di aggiungere un dipartimento, allora questo deve essere unico (in modo che abbia solamente un gestore).

* Se non c’è la freccia marcata, come in questo caso, questa opzione è utile, siccome non è obbligatorio avere un valore per ogni dipartimento.



Un altro modo potrebbe essere:

```sql
CREATE TABLE Departments(
	did INTEGER,
    dname CHAR(20),
    budget REAL,
    ssn CHAR(11),
    since DATE,
    PRIMARY KEY (did),
    FOREIGN KEY(ssn)
    	REFERENCES Employes
);
```

In questa opzione si utilizza un modello di dipartimento leggermente diverso, aggiungendo tutti gli attributi (in più a quelli che precedentemente aveva) come:

* il dipendente che lo gestisce
* la data da quando questo dipendente ha gestito il dipartimento

In questo modo ho un attributo questo dipendente come attributo e di fatto trovo la chiave sempre come codice del dipartimento, in più però bisogna indicare `ssn` cos’è. Per questo si deve indicare `FOREIGN KEY (ssn)` in modo che si aggiunga questo valore dalla chiave primaria dei dipartimenti.

Questa modalità è utile siccome si può avere un dipendente al massimo nella gestione. 

La differenza di questo approccio rispetto al modello precedente è che tutte le volte che si crea un nuovo `Departments` si deve aggiungere anche un `Employes` tutte le volte. Questo fatto potrebbe far sembrare si violi il vincolo dato dalla freccia sottile (che implica che non sia per forza esistente). Questo si risolve perché tutti questi attributi (eccetto la chiave primaria) possono essere `null` e di fatto questo impone che si può togliere la necessità di avere per forza un manager. 

Di base questo modello impone l’utilizzo di `null` (cosa non sempre positiva). 

Se `ssn` è `null` non si sta violando il vincolo di `FOREIGN KEY` perché quando questo attributo è `null` questo vincolo viene ignorato.



I vantaggi della prima soluzione sono che:

* non utilizza i `null`
* (più concettuale) separo le entità che partecipano alla relazione dalla relazione stessa

Gli svantaggi della prima soluzione sono che:

* guardando alla tabella `Managers` che rappresenta la relazione non si riesce a ottenere le informazioni sui dipartimenti, ma solo gli `id` dei dipartimenti.
  * Chiaramente una ricerca di questo tipo è più dispendioso per l’accesso al disco



I vantaggi della seconda soluzione sono che:

* gli accessi al disco sono molto ridotti, siccome nella tabella `Departments` ci sono tutte le informazioni che mi interessano

Lo svantaggio della seconda soluzione è che:

* non ho più una tabella per le relazioni, quindi l’entità viene sovraccaricata di informazioni in più che non sono veramente essenziali per la relazione



L’ultima relazione è la **freccia marcata**: il dipartimento *deve* per forza avere un unico dipendente che gestisce.

<img src="image/image-20211125115538322.png" alt="image-20211125115538322" style="zoom:50%;" />

L’opzione 1 proposta precedentemente non è più fattibile, ma in questo modo si indica la relazione tra i dipartimenti e indico che il valore del dipartimento non deve per nessun caso essere nullo:

```sql
CREATE TABLE Departments(
	did INTEGER,
    dname CHAR(20),
    budget REAL,
    ssn CHAR(11) NOT NULL,
    since DATE,
    PRIMARY KEY (did),
    FOREIGN KEY(ssn)
    	REFERENCES Employes
    	ON DELETE NO ACTION
);
```

Si noti quindi l’utilizzo delle keyword `NOT NULL` e `ON DELETE`: questo serve in primo luogo a obbligare l’esistenza di un manager durante la creazione di un nuovo dipartimento, la seconda impone che la `DELETE` su un `Employees` non sarebbe permessa, se questo è un `Managers` di un dipartimento.



Le relazioni uno-a-uno (**doppia freccia marcata** in un relazione) è impossibile rappresentarle completamente, ma si può fare una cosa particolare tipo:

```sql
CREATE TABLE Managers(
	ssn CHAR(11),
    did INTEGER,
    since DATE, 
    PRIMARY KEY (did),
    UNIQUE (ssn), -- per impedire lo stesso manager su due diversi dipartimenti 
    FOREIGN KEY (ssn)
    	REFERENCES Employees,
    FOREIGN KEY (did)
    	REFERENCES Departments
);
```

In questo modo per non riesco a mantenere la riga marcata, perché non c’è nessun modo in questo caso per indicare l’obbligo di `Managers` su un dipartimento (quindi di fatto, se non inserisco nella tabella centrale può ancora funzionare correttamente e non c’è modo per bloccare che ciò avvenga).



Il vincolo di **partecipazione totale** (la linea marcata senza freccia, che obbliga alla presenza di tutti i managers). Questo modello non può correttamente essere gestito dal modello relazionale. Questo non significa che non si possa ottenere, ma che debba essere gestito dal codice di alto livello a cui è collegata l’applicazione.

Unica implementazione che può curare qualche vincolo in questo modello è:

```sql
CREATE TABLE Managers(
	ssn CHAR(11),
    did INTEGER,
    since DATE,
    PRIMARY KEY (did),
    FOREIGN KEY (ssn),
    	REFERENCES Employees,
	FOREIGN KEY (did)
    	REFERENCES Departments
);
```



### Weak Entity set

La mappatura di un **weak entity set** (le cui entità esistono solo finché esistono le entità a cui sono connesse).  Di fatto questo significa che se si perde il valore da cui si dipende, allora bisogna eliminare anche l’entità subordinata a catena.

 ![image-20220115104517938](image/image-20220115104517938.png)



Quindi una relazione di questa modalità si mappa con una freccia marcata, attraverso l’opzione due:

```sql
CREATE TABLE Dependants(
	dname VARCHAR(20),
    age INTEGER,
    cost REAL,
    ssn CHAR(11) NOT NULL, -- è necessario ci sia almeno un dipendente che gli da la polizza
    PRIMARY KEY (dname, ssn)
    FOREIGN KEY (ssn) 
    	REFERENCES Employees,
    ON DELETE CASCADE -- weak entity set
);
```

L’istruzione `ON DELETE CASCADE` significa che se si elimina un Dipendente nella tabella `Employees` allora è necessario rimuovere anche gli elementi che sono presenti in questa tabella che dipendono dal dipendente eliminato.

Di fatto quindi questa implementazione si ottiene esattamente con un la relazione di riga marcata e l’istruzione `ON DELETE CASCADE`.



### Esempio

<img src="image/image-20211125141709704.png" alt="image-20211125141709704" style="zoom:77%;" />

```sql
CREATE TABLE AzFarm(
	name VARCHAR(20),
    phone VARCHAR(20), -- così non succede nulla se ha davanti lo zero
    PRIMARY KEY(name),
);
CREATE TABLE Farmacie(
	name VARCHAR(20),
    phone VARCHAR(20),
    addr VARCHAR(50),
    PRIMARY KEY (name)
);
CREATE TABLE Medicine( -- Weak Entity set
	tradeName VARCHAR(20),  -- nome commerciale
    formula VARCHAR(50),
    AzFarmName VARCHAR(20) NOT NULL, -- Nome az Farma, deve averla
    PRIMARY KEY(tradeName, AzFarmName), -- La medicina è identificata univocamente dal nome dell'azienda produttrice e il nome della medicina
    FOREIGN KEY (AzFarmName) REFERENCES AzFarm, -- attributo AzFarmName si collega alla tabella AzFarm
    ON DELETE CASCADE -- Per definire il weak entity set
);
-- Relazione tra farmacia e azFarm
-- Relazione molti a molti
CREATE TABLE Contratti(
	Fname VARCHAR(20),  -- nome della farmacia
    AFname VARCHAR(20), -- nome Az Farm
    supervisor VARCHAR(20), 
    startDate DATE, -- date mese,giorno, anno. Se si vuole precisione al secondo serve DATETIME
    endDate DATE,
    text TEXT, -- questo tipo non ha dimensione chiara, di solito il DBMS non riesce a ottimizzare l'accesso al disco
    PRIMARY KEY (Fname, AFname), -- coppia univoca
    FOREIGN KEy (Fname) REFERENCES Farmacie,
    FOREIGN KEY (AFname) REFERENCES AzFarm
);
-- Relazione molti a molti
CREATE TABLE Vendita(
    Fname VARCHAR(20),
    tradeName VARCHAR(20),
    AFName VARCHAR(20),
    price REAL,
	PRIMARY KEY(Fname, tradeName, AFName),
    FOREIGN KEY (Fname) REFERENCES Farmacie,
    FOREIGN KEY (tradeName, AFName) REFERENCES Medicine,
);
CREATE TABLE Dottori(
	ssn CHAR(11), -- CHAR perchè quantità fissa e inviolabile
    name VARCHAR(20),
    -- speciality AS ENUM ('Chirurgia', 'Neurologia', '...', ..), -- tipo per creare un valore con solo con questi valori. Non detto che funizioni, ma con il TYPE (sotto) va meglio
   --  speciality Spec,
   -- in questo caso basta:
    speciality VARCHAR(20),
    -- non si può rappresentare la linea marcata
    stratDate DATE,
    PRIMARY KEY(ssn)
);
-- type ENUM per Spec, non usato alla fine, ma vale come esempio
CREATE TYPE Spec ENUM('Chirurgia', 'Neurologia', ' ... ', ...);
CREATE TABLE Pazienti(
	ssn CHAR(11),
    name VARCHAR(20),
    addr VARCHAR(50),
    birthday DATE, 
    -- un paziente è curato solo da un dottore => aggiungo un altro attributo. NOT NULL perché deve avere il dottore per forza
    doctssn CHAR(11) NOT NULL,
    -- Se questo fosse stato senza NOT NULL allora il vincolo sarebbe stato solo la freccia sottile. Il valore se viene a mancare è definito come `null`.
    PRIMARY KEY (ssn),
    REFERENCES KEY(doctssn) REFERENCES Dottori
);
CREATE TABLE Prescrizioni(
	pssn CHAR(11),
    dssn CHAR(11),
    tradename VARCHAR(20),
    afname VARCHAR(20),
    quantity INTEGER,
    date DATETIME,
    PRIMARY KEY (pssn, dssn, tradename, afname),
    FOREIGN KEY (ssn) REFERENCES Pazienti,
    FOREIGN KEY (dssn) REFERENCES Dottori,
    FOREIGN KEY (tradename, afname) REFERENCES Medicine
);
```

### Gerarchie ISA

I modelli ISA servono per avere delle entità che hanno degli attributi in comune, mentre invece si ottiene un’altra serie di valori diversi per ogni entity-set.

Lo schema ER di questa gerarchia, si presenta in questo modo:

![image-20220115105844117](image/image-20220115105844117.png)

Il concetto è che ci sia overlap di attributi, ovvero alcuni elementi che sono in comune su più entità. Lo schema che permette di modulare uno schema con ISA è:

```sql
CREATE TABLE Employees (ssn, name, lot);
CREATE TABLE Hours(
ssn, hw, hwork
	FOREIGN KEY (ssn) REFERENCES Employees, -- stratagemma per indicare che sono dipendenti e quindi avere una matricola proveniente dalla tabella Employees
)
CREATE TABLE ContrShip(
ssn, contractId
	FOREIGN KEY (ssn) REFERENCES Employees, -- in questo modo si riescie a indicizzare le matricole dei dipendenti, con un codice di una matricola già presente, con le informazioni necessarie.
    -- così facendo, si ottiene un insieme con solo i dipendenti 
)
```

In questo modo però diamo la possibilità a un `Employeer` di non essere ne sotto contratto o pagamento ad ore.

Poi per capire, guardando solo la tabella di `Employees`, si aggiunge un attributo `Hourly_Emps` o `Contact_Emps` che indica se ha o meno questo valore. Oppure si può fare un unico campo con un valore di `ENUM` tra i valori (`type ENUM(‘b’, 'c‘, ‘e’))`. Il vantaggio di questo modello è che si ottiene la possibilità di non avere mai overlap.

Lo svantaggio di questo modello è che si hanno un sacco di dati da mantenere costante e soprattutto rendono i dati molto facilmente sbagliati. Per questo motivo serve un’applicazione ad alto livello che si rompa la compatibilità e la verità dei dati.

Se si ha cover e overlap si utilizzano 2 sole tabelle: quelle dei figli. Non considero assolutamente la classe padre e creo solo quella dei figli. In questo modo non esiste la possibilità che ci sia un dipendente di un tipo che non comprendo. Lo svantaggio è che non esiste la possibilità di avere `ssn` univoci tra le due tabelle.

Il vantaggio di questo modello è che posso fare una ricerca su un tipo di lavoratore cercando solo su una tabella. Lato negativo: se devo cercare fra tutti i lavoratori allora sono inefficiente.



### Aggregazioni

<img src="image/image-20211125155927045.png" alt="image-20211125155927045" style="zoom:50%;" />



Di fatto l’aggregazione non viene mappata, ma solo la tabella `Managers`. Creo quindi questa tabella:

```sql
CREATE TABLE Managers(
	ssn VARCHAR(20),
    did VARCHAR(20),
    since DATE,
    PRIMARY KEY(ssn, did),
    FOREIGN KEY (ssn) REFERENCES Employees,
    FOREIGN KEY (did) REFERENCES Departments
);
```

Adesso questa relazione viene portata allo stesso livello di entità:

```sql
CREATE TABLE Controls(
	FoundsId CHAR(11),
   	ssn VARCHAR(20),
    did VARCHAR(20),
    dname VARCHAR(20),
    PRIMARY KEY (FoundId, ssn, did),
    FOREIGN KEY (FoundId) REFERENCES Found,
    FOREIGN KEY (ssn, did) REFERENCES Managers
);
```



### Esercizio

![image-20211125160544273](image/image-20211125160544273.png)

```sql
-- Implementazione logica di ER
CREATE TABLE Professori(
	ssn CHAR(11),
    name VARCHAR(20),
    age DATE,
    speciality AS ENUM('Chirurgia', '..'), -- se si hanno specifiche migliori, seguire le specifiche
    rank VARCHAR(20),
    PRIMARY KEY (ssn),
);
CREATE TABLE Departments(
    number INTEGER,
	name VARCHAR(20),
    office VARCHAR(20),
    profSsn CHAR(11) NOT NULL, -- richiede che ci sia un professore che gestisce
    PRIMARY KEY (number),
    FOREIGN KEY (profSsn) REFERENCES Professori
);
CREATE TABLE WorksFor(
	dept_num INTEGER,
    prof_ssn CHAR(11),
    time DATE,
    PRIMARY KEY (dept_num, prof_ssn),
    FOREIGN KEY (dept_num) REFERENCES Departments,
    FOREIGN KEY (prof_snn) REFERENCES Professori
);
CREATE TABLE Project(
	number INTEGER,
    stratDate DATE,
    endDate DATE,
    sponsor VARCHAR(20),
    budget INTEGER,
    prof_ssn CHAR(11) NOT NULL,
    PRIMARY KEY(number),
    FOREIGN KEY (prof_ssn) REFERENCES Professori
);
-- Professore lavora a uno o più progetti, un progetto è definito da uno o più professori
CREATE TABLE WorkP(
	porf_ssn CHAR(11),
    proj_num INTEGER,
    PRIMARY KEY(prof_ssn, proj_num),
    FOREIGN KEY (prof_ssn) REFERENCES Professori,
    FOREIGN KEY (proj_num) REFERENCES Project
);
CREATE TABLE GradStud(
	ssn CHAR(11),
    name VARCHAR(20),
    birthday DATE,
    degree VARCHAR(20), -- a meno che non ci sia un entity set, ma questo è il progetto
    dept_num INTEGER NOT NULL,
    tutor_ssn CHAR(11) NOT NULL,
    PRIMARY KEY (ssn),
    FOREIGN KEY (dept_num) REFERENCES Departments,
    FOREIGN KEY (tutor_ssn) REFERENCES GradStud,
    -- Studente potrebbe essere tutor di se stesso? Si
    -- Come faccio aggiungere uno studente, siccome ne serve uno per aggiungere uno stundete.
    -- Modello per risolvere: - creare una ISA - creo uno strudento con tutti i parametri `null`, in questo modo posso chiederlo nel primo studente che inserisco
    -- Modello forse più elegante: 
    -- - tabella Tutoring con `PRIMARY KEY` su `student_ssn`, in questo modo non faccio una relazione strana con un sacco di studenti che fanno da tutor
   	-- - faccio una relazione senza il `NOT NULL` su tutor_ssn
    -- - 
);
```

in generale è consigliabile utilizzare una tabella per le entità, ma non è sempre detto.

Attenzione di non abusare delle *weak entity* che rendono tutto il database più instabile.



## SQL: Data Query Language

Questa seconda parte del linguaggio è il segmento di questa sezione per fare ricerche sul DBMS.

Questo linguaggio, essendo dichiarativo, non spiega come fare le interrogazioni, ma dicono solo il risultato che si vuole ottenere. Il DBMS implementa la richiesta in modo da fornire una risposta nel modo più veloce e reattivo possibile.

Un **query** in SQL ha una struttura di questo tipo (con 3 clausole generalmente, `select`, `from` e `where`):

```sql
select Attribute...Attribute
from Table...Table
where Condition
```

* Con l’operatore `select` si può avere l’operazione $\pi$ (la selezione delle colonne).

  * Se si vuole la rimozione dei duplicati in questa operazione (questa azione non viene fatta di default perché computazionalmente impegnativa).

    ```sql
    select distinct surname, city
    from employees
    ```

  * Se si vuole ripetere delle colonne o indicare le colonne della tabella del risultato si utilizza questa keyword e si indicano tutte le colonne.

  * per semplificare il linguaggio quando si vogliono ottenere tutti gli attributi allora si indica. Inoltre la mancanza di `where` indica che si tengono tutti gli attributi, senza nessun tipo di proiezione.

    ```sql
    select *
    from employees
    ```

  * se si vogliono operare delle funzioni matematiche sulle colonne allora si può aggiungere in questa sezione del linguaggio:

    ```sql
    select name, age, income, income*2 as doubleIncome
    from person
    ```

* si può operare un renaming sulla tabella con l’operatore `as`:

  ```sql
  select p.name, p.income
  from person as p
  where p.age < 30;
  
  -- oppure cambiare valore degli attributi
  select p.name as name, p.income as income
  from person as p
  where p.age < 30
  ```

  * Queste interrogazioni restituiscono gli stessi risultati, solo con la differenza che le colonne sono rinominate diversamente.

* `LIKE` condition:

  * Serve per trovare delle tuple il cui nome è “circa” qualcosa.

  * Sintassi:

    ```sql
    select *
    from person
    where name like 'A_d%';
    ```

    Questa richiesta indica che vogliamo solo le righe il cui campo `nome` ha prima lettera `A`, poi una lettera qualsiasi, il carattere `d` e poi tanti altri caratteri che non ci interessano. Esempio di match: `Andrea`, `Asdrubale`.

  * `age is null` => modello per indicare il comportamento in caso di valore `null` sul campo richiesto.

    ```sql
    select *
    from person
    where name like 'A_d%' or age is null;
    ```

    Anche perché il DBMS di solito è conservativa e quindi ti restituisce solo le informazioni di cui è certo (nel caso senza `is null` non si restituisce nel caso di `null`).

    * si utilizza `is`, che è completamente diverso da `age = null`. Questo si diversifica perché se nel processo si trova un valore  `null` e si esegue un confronto, allora si ottiene  `null==null` che ottiene `null` come risposta (Errore al DBMS che non sa come trattare l’informazione). Per questo motivo si utilizza `is`, ovvero si cerca se si ottiene nel campo il simbolo che rappresenta `null`.



### Query non di base

La `join` si esegue specificando più tabelle nella clausola `from`:

* Natural Join oppure $R1 \Join R2$:

```sql
select R1.A1, R2.A4
from R1, R2
where R1.A2 = R2.A3
```

* se si vuole fare *join* multiple si utilizza sempre questo modello, con indicate tutte le tabelle necessarie per l’operazione e poi si indicano i confronti da fare con la *keyword* `where`.

* Se si vuole fare una join sulla stessa tabella allora serve rinominare la tabella. In questo caso la *keyword* `as` è facoltativa: 

  ```sql
  select X.A1 as B1 ...
  from R1 X, R2 Y, R1 Z
  where X.A2 = Y.A3 and ... 
  ```

* Per rendere le interrogazioni più leggibile in SQL, si possono fare le *join* in una clausola a parte chiamata `join`.

  ```sql
  select isFather.child, father, mother
  from isMother, isFather
  where isFather.child = isMother.child
  -- con la join
  select c.name, c.income, p.income
  from person p join isFather t on p.name=t.father
  	join person c on t.child=c.name
  where c.income > p.income
  ```



La `left outer join` e la `right outer join` (con `outer` lasciato opzionale sempre). 

```sql
-- left outer join
select isFather.child, father, mother
from isFather left join isMother on isFather.child = isMother.child
```

pLa `full outer join` viene sputato tutto.

#### riassunto

$isFather \Join isMother$

```sql
-- in SQL explicit
select isFather.child, father, mother
from isMother join isFather on isFather.child = isMother.child

-- in SQL natural join
select isFather.child, father, mother
from isMother natural join isFather
```





### Esempio:

Si ha una base di dati con `TABLE person(nome:string, age: int, income:int)`, `TABLE isMother(mother:string, child:string)`, `TABLE isFather(father:string, child:string)`. 

1. Vogliamo sapere le persone che sono più giovani di 20 anni:

   ```sql
   select person.name, person.income
   from person
   where person.age < 30
   ```

   Questa interrogazione prende`name` e `income`, provenienti dalla tabella `person`, con la vera selezione operata da `where`, che seleziona le righe dove l’età delle persone è minore di 30.

2. Voglio una tabella con due colonne per lo stipendio tra i 20 e i 30:

   ```sql
   select name, income, income as repeatedIncome
   from person
   where income >= 20 and income <= 30
   ```

   

