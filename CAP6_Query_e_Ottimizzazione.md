# Query e Ottimizzazione

Il problema dell’ottimizzazione delle query viene introdotto quando si lavora con un database sempre più grande, ovvero che contiene sempre più dati.

Ci sono principalmente due tipi di memoria disponibili al momento:

* la memoria **RAM**: caratterizzata da bassissimo tempo di latenza per leggere e scrivere dati (anche fino a 3GB), ma allo stesso modo da pochissimo spazio a disposizione (nell’ordine di un centinaio di GB)

* la memoria secondaria: di solito basata su *hard-disk* di tipo meccanico. Proprio per la configurazione e costruzione di questo elemento (che lavora in modo meccanico) è facile ottenere dei tempi di latenza molto più alti (si indicano di solito intorno ai 500MB/sec in lettura/scrittura).

  Di solito l’hard-disk è costruito in delle *traccie* concentriche che immagazzinano i dati. Queste contengono delle sezioni, chiamati *settori* (di solito sono da 4kb) che sono la sezione di memoria fisica più piccola disponibile sul supporto.

  I settori sono utili al supporto siccome quando si richiede un dato in un determinato luogo di memoria, è molto probabile che i dati intorno vengano richiesti immediatamente dopo. Di fatto quindi con un unica lettura recupera tutto il contenuto del settore e le richieste successive del contenuto di quel settore saranno con un tempo di latenza praticamente nullo.

  Solitamente però la dimensione media di un settore fisico non è abbastanza grande da permettere di memorizzare una tupla del DBMS. Per questo motivo il DBMS utilizza un **blocco** o **pagina**, che generalmente occupano 8/16kb su disco e di conseguenza sono 2/4 settori.

  Quando un postgres o un DBMS accederà al disco allora leggerà un intera pagina, utilizzando la ram come settore di memoria più veloce dove trattenere temporaneamente i dati copiati. In questo modo si ottiene una lettura immediata anche delle tuple precedenti/successive che solitamente permette un miglioramento delle prestazioni. 

Solitamente siccome il database tende ad essere molto grande nel tempo, generalmente si tende a mantenere il database in memoria secondaria e copiare temporaneamente i dati in memoria primaria se necessario (si prende come immediato il tempo di lettura e scrittura dalla memoria centrale, mentre l’effettivo costo di recuperare i dati dalla memoria secondaria è il tempo necessario che influisce sulla richiesta al DBMS).  

Per misurare il tempo di esecuzione delle operazioni quindi è necessario sapere quante operazioni di lettura o scrittura di pagine avvengono sul disco, in questo modo è possibile determinare l’effettivo **modello di costo**, ovvero la stima del tempo necessario per eseguire un operazione nel database.  



Adesso prendiamo un database che contenga una sola tabella `R`, così costruita: 

```sql
R (id INTEGER, num INTEGER, city VARCHAR, age VARCHAR); 
```

Il DBMS calcola il valore di spazio necessario ad ogni tupla e, assumendo siano tutte dimensioni uguali, indichiamo con $t_R$ questo valore.

Il DBMS ha a disposizione una serie di informazioni:

* dimensione delle pagine
* dimensione di tutta la base dati

In questo modo il DBMS ha a disposizione sempre le informazioni necessarie a recuperare le informazioni cercate e anche le informazioni di prima e ultima pagina del db.

Il DBMS utilizza questi costi per scegliere il tipo di query da utilizzare, in modo da avere l’implementazione migliore. 

Tutte le strutture presentate avranno dei lati positivi o negativi, lo scopo è di travare la struttura che meglio si adatta alle necessità. Inoltre da quando si inizia a parlare di file ordinati, allora è necessario aver presente qual’è il valore per cui ordinare, ovvero il valore della tupla per cui ci si aspettano più interrogazioni in assoluto.



## Heap File

La tupla viene inserita nel database attraverso la creazione di una pagina. Di solito non è conveniente che si abbia una tupla a metà tra due pagine, siccome il tempo necessario a recuperare due pagine non conviene rispetto alla perdita di un questo spazio. 

Questo modo di organizzare le informazioni viene chiamato **HEAP-FILE** (mucchio):  i file non sono organizzati in nessun modo, senza nessun tipo di relazioni di ordine, ma semplicemente inseriti come vengono letti.

Calcoliamo adesso il tempo necessario per eseguire delle operazioni di base, calcolando come costo ogni lettura di ogni pagina (siccome il collo di bottiglia è l’accesso al disco). Il numero di pagine presenti sul disco viene indicato con $p_R$:

* operazione di **scan** del file (ovvero visualizzazione dell’intero db con recupero di tutte le tuple) che corrisponde a questo comando SQL:

  ```sql 
  select * from R;
  ```

  Per svolgere questa operazione ci si aspetta un costo unitario uguale al numero delle pagine, quindi $\text{costo} = p_R$.

* operazione di **equality search**, che è rappresentato da questo codice SQL: 

  ```sql
  select * from R where city = 'Trento';
  ```

  Le letture sul disco saranno ancora una volta $p_R$, infatti dovrà essere controllato ogni file presente nel db siccome non c’è modo di sapere se esiste un file con all’interno quell’informazione in un determinato posto del disco a priori.

* operazione di **insert**, che rispecchia il codice SQL di questo tipo:

  ```sql
  insert into R values ( --- tupla --- );
  ```

  Il costo di questa operazione dipende se l’ultima pagina esistente ha ancora spazio a disposizione per accettare una tupla. Se questo controllo è positivo allora si inserisce la tupla, e poi scrivo la pagina modificata dove era posizionata precedentemente.

  Queste due operazioni (recupero dell’ultima pagina, scrittura dell’ultima pagina) ha costo unitario $2$.

  Se invece, dopo aver letto l’ultima pagina, non ho spazio dove salvare la mia tupla, allora scarto questa pagina, creo un altrà pagina e la aggiungo al disco infondo al file. 

  Il costo anche in questo caso è $2$ (si legge l’ultima pagina, si scrive la pagina successiva).

   Questo costo inoltre può cambiare leggermente se è necessario svolgere un controllo di chiave primaria, ovvero verificare l’unicità di un valore in tutta la base dati prima dell’inserimento. Questo problema comunque non verrà trattato a questo punto, ma più avanti con gli indici.

* operazione di **delete** (eliminazione di una tupla), che corrisponde a questo codice SQL

  ```sql
  delete from R where city = 'Trento'; -- è una query normale, solo che invece di select si utilizza delete
  ```

  Per trovare tutte le persone che hanno come `city = 'Trento'` è necessario uno scan dell’intero db. Non è però necessario scriverle tutte, ma solo le pagine che contenevano questa informazione, che in ram è stata cancellata e che quindi deve essere riscritta (aggiornata) sulla memoria secondaria. Di fatto quindi ci saranno tante scritture come tutti gli elementi del db, ma scritture solo quante le tuple che corrispondono a quelle cercate.

  Per questo motivo il modello di costo della scrittura delle tuple che contengono `'Trento'`, e si indica in questo modo: $|R_{city='Trento'}|$. Dove le $||$ indicano la cardinalità (il numero di tuple) della relazione $R$ i cui elementi contengono quel valore. 

  Di fatto il costo totale di questa operazione è $p_R+ |R_{city = 'Trento'}|$. Questo costo è il valore peggiore, che indica che nel caso più sfavorevole tutte le tuple da scrivere sono tutte su pagine diverse. 

  Se si lasciano delle pagine vuote il DBMS ha diversi metodi per tener traccia, riempire in un secondo momento oppure altri modelli di implementazione. Per questo motivo tutte queste implementazioni di operazioni che saranno svolte da DBMS in un secondo momento per mantenere la consistenza della base di dati non verranno considerate in questo corso (nonostante poi nella realtà ci siano).

  Solitamente il DBMS mantiene una serie di statistiche per avere accesso a questi calcoli in modi automatici. Molto più spesso invece fa delle assunzioni basate sulla cardinalità dell’intera base di dati e sulla disposizione omogenea dei dati (il DBMS presume omogenea per semplicità). Fatte queste due assunzioni, si ottiene che il valore medio che meglio rappresenta la cardinalità delle tuple contentente `'Trento'` è rappresentabile in questo modo:
  $$
  |R_{city = 'Trento'}| = \frac{|R|}{|R_{city}|}
  $$
  Questo valore però non è molto comodo, siccome frazionario. Di conseguenza generalmente si prende l’intero superiore: 
  $$
  \ulcorner |R_{city = 'Trento'}| \urcorner = \ulcorner \frac{|R|}{|R_{city}|} \urcorner
  $$
  Un altro modo di vedere questa formula è spezzandola:
  $$
  |R_{city = 'Trento'}| = \frac{1}{|R_{city}|} * |R|
  $$
  In questo modo: 
  $$
  \text{Selectivity Factor} = \frac{1}{|R_{city}|}
  $$
  

  Questo valore indica il numero di tuple sull’intera collezione che contiene quel determinato valore. Di fatto si dice che:

  * il *selectivity factor* è molto selettivo se rimangono poche tuple dopo questa operazione (quindi il valore è *molto basso*)
  * il *selectivity factor* è molto poco selettivo se rimangono molte tuple dopo questa operazione (quindi il valore è *molto alto*).
  * 

  In generale quindi si può semplificare questo costo in questo modo: 
  $$
  p_R + |R_{city = 'Trento'}| = p_R + \ulcorner f* |R| \urcorner
  $$
  dove $f$ è il *selectivity factor* (che di solito è un valore con la virgola, quindi si prende l’intero superiore)



### Heap-file: Vantaggi e Svantaggi

Questo modello di dati è ottimo per la *raccolta di dati*, perché la memorizzazione dei dati è a tempo costante. Inoltre ha un costo di gestione minimo.

Al contrario questo modello non sarà mai efficiente per le ricerche, perché l’equality search avrà sempre un costo uguale al numero di tuple del database.



#### Heap file: Riassunto dei costi

Prendiamo $p_R$ il numero delle pagine sul disco, $t_R$ lo spazio necessario per ogni tupla sul disco.

* operazione di **scan**: $p_R$
* operazione **equality-search**: $p_R$
* operazione **insert**: $2$
* operazione **delete**: $p_R + |R_{city = 'Trento'}| = p_R + \ulcorner f* |R| \urcorner$





## Sorted File

In questo tipo di file si distingue dall’heap file perché viene mantenuto un ordine delle tuple inserite nel file. In questo modo la ricerca è possibile in tempi più moderati, anche quando il numero delle tuple è molto grande. Al contrario si perde più tempo nella fase dell’inserimento dei dati.

Il sorted file ha un grandissimo inconveniente che sono tutti i costi di gestione del file che deve sempre rimanere ordinato. Di fatto questi costi sono così importanti che solitamente è meglio l’implementazione tramite *heap-file* invece che con il *sorted-file*, soprattutto se si parla di molti inserimenti.

I costi di questo tipo di file sono:

* operazione di **scan**: $p_R$ (non si migliora il costo di scan, siccome devo comunque scorrere tutto il database)

* operazione di **equality search**, che corrisponde ai comandi SQL:

  ```sql
  select * from R where city = 'Trento';
  ```

  Siccome il file è ordinato (mettiamo in questo caso ordinato su questo valore) allora possiamo applicare la ricerca binaria per trovare l’elemento.

  La ricerca binaria è l’algoritmo che lavora con complessità $log_2(n)$, dove $n$ è il numero di elementi contenuti nell’array. Lavora facendo un confronto con il valore centrale dell’insieme e in questo modo riesce a decidere se operare un altra volta sul sottoinsieme di destra o di sinistra. 

  Di base questo algoritmo permette di posizionarci sempre sulla prima pagina che contiene questo elemento cercato, e quindi di fatto al costo per la ricerca binaria si aggiungono tutti gli elementi che seguono e che rispondono sempre a questa uguaglianza.

  Il costo di questa operazione quindi diventa:
  $$
  \ulcorner log_2 (p_R) \urcorner + \ulcorner \frac{|R_{citta = 'Trento'}|}{\llcorner \frac{p}{t_R} \lrcorner} \urcorner
  $$
  che sono le due operazioni:

  * lettura di tutte le pagine finché non si ottiene il primo match

  * lettura di tutte le pagine consecutive (da ricordare che ogni pagina può contenere più di una tupla)

    * Devo fare uno *scan* pagina per pagina finché non trovo la prima pagina che contiene una tupla non con questo parametro

    * il valore di questo numero dipende dal numero di tuple che entra in una pagina:

      * so che una pagine $p$, con la dimensione di una tupla $t_R$

      * prendo $\llcorner \frac{p}{t_R} \lrcorner$, ovvero il valore massimo di tuple all’interno di una pagina. Di questo calcolo si prende il valore minore siccome non si possono avere delle tuple a cavallo di due pagine. 

      * con questo valore si sa il numero totale di pagine da leggere con 
        $$
        \frac{\text{#P con valore cercato}}{\text{#tuple per pagina}} = \frac{|R_{citta = 'Trento'}|}{\llcorner \frac{p}{t_R} \lrcorner}
        $$

  Questo modello di ricerca permette di avere anche un modello di **range-search** molto efficiente. Infatti se si vuole ad esempio trovare tutte le persone fra due età, allora si trova la minore e poi si leggono tutte le pagine che contengono tuple che non superino l’intervallo maggiore.

* operazione di **insert**:

  il costo di questa operazione cambia dall’heap file, siccome ogni volta che si inserisce una tupla all’interno del database è necessario posizionarla nel punto giusto, in modo da mantenere il file ordinato. 

  Il costo di questa operazione è:
  $$
  \ulcorner log_2(p_R) \urcorner + 1
  $$
  che dipende da queste due operazioni:

  * si effettua una ricerca binaria per trovare il punto in cui andrebbe inserita la tupla
  * si legge la pagina corrente:
    * se la pagina è abbastanza vuota da poter contenere un’altra tupla, allora scrivo con costo $1$. (costo unitario diventa $log_2(p_R) + 1$, ipotesi migliore)
    * se la pagina invece non ha spazio bisognerebbe contare anche tutto il costo necessario a spostare avanti le pagine che contengono valori superiori al nostro rispetto all’indice. Questo costo non è nullo, anzi è **piuttosto dispendioso** ma noi non lo tratteremo. Di fatto questo costo però è quello che nella pratica solitamente impone di non utilizzare il sorted-file quasi mai.

  Di fatto noi prendiamo come costo di questa operazione l’operazione più favorevole possibile.

* operazione di **delete**:
  $$
  \ulcorner log_2(P_R) \urcorner + 2 * \ulcorner \frac{|R_{city = Trento}|}{\ulcorner\frac{P}{t_R}\urcorner}\urcorner
  $$
  Questo valore dipende da:

  * Il costo per trovare la pagina da eliminare ha costo logaritmico.
  * il numero di pagine che so essere occupate con il valore cercato. Unica differenza che questa volta tutte queste pagine devono anche essere riscritte (quindi costo $1+1$, una lettura e una scrittura).



### Sorted file: Riassunto dei costi

Prendiamo $p_R$ il numero delle pagine sul disco, $t_R$ lo spazio necessario per ogni tupla sul disco.

* operazione di **scan**: $p_R$
* operazione **equality-search**: $\ulcorner log_2 (p_R) \urcorner + \ulcorner \frac{|R_{citta = 'Trento'}|}{\llcorner \frac{p}{t_R} \lrcorner} \urcorner$
* operazione **insert**: $\ulcorner log_2(p_R) \urcorner + 1$
* operazione **delete**: $\ulcorner log_2(P_R) \urcorner + 2 * \ulcorner \frac{|R_{city = Trento}|}{\ulcorner\frac{P}{t_R}\urcorner}\urcorner$



## Riduzione dei costi: sistemi basati sugli indici

I sistemi basati su file hanno alcuni problemi molto seri rispetto a dei costi di gestione della consistenza del file, soprattutto i *sorted-file* (che necessitano praticamente di essere ricostruiti ogni volta che si inserisce una tupla). 

Per questo motivo si passa a un nuovo metodo di memorizzare i dati, chiamati **indici**. Questi metodi permettono di avere dei file ordinati, che però non necessitino di costi di “manutenzione” dei file così importanti.

Il funzionamento ad alto livello di questo metodo è che si indicano i dati su un file esterno a quello che li contiene. Questo file verrà chiamato indice (prende spunto dall’indice analitico dei libri, quello che segnala dove vengono trovate delle parole specifiche all’interno del libro).

Di fatto questo modello permette di ottenere le informazioni sulle pagine che sto cercando scansionando un semplice file. Esistono due macro-tipi di implementazioni di questo modello:

* prima basata sugli *hash*

  * ci sono tante implementazioni, la più famosa è la **hashtable**

* seconda basata sugli *alberi*

  * la più famosa è basata sui **B+-tree**
  * Postgress utilizza questo modello, in particolare i B+ tree. 

  

Il modo più facile e utile da vedere è come postgress utilizza questa struttura e come funziona.

Il DBMS utilizza due file: il *data file* (contiene i dati veri e propri), l’*index-file* (contiene l’indice analitico dei file). Il file contenente gli indici di base conterrà il puntatore a dove trovare il dato completo e il valore della propria chiave presa ad indice.

Quando si vuole inserire una tupla allora:

* nel *datafile* inserisco in fondo (come fosse un heap file)
* nell’*index-file* inserisco un puntatore alla pagina che ha salvato la tupla

Adesso si vuole inserire un altro valore all’interno del database:

* si inserisce come un heapfile sul datafile
* si inserisce un nuovo puntatore alla pagina dov’è salvata questa tupla nell*index file*. Questa volta però si procede a inserire questo puntatore in modo ordinato rispetto alla lista che questo file contiene. 

Si continua in questo modo per ogni inserimento. Di fatto anche se si inseriscono delle tuple che contengono un dato minore a quello già inserito, si inserisce tranquillamente in fondo al *data-file* e poi si inserisce un valore nell’indice ordinato.

Poi succede che si inseriscono tanti dati da avere necessità di aggiungere una pagina all’*index-file*. Quindi si spezza l’indice a metà e si continua a lavorare. Questo indice però continuando in questo modo diventerà una lunga catena e quindi l’operazione di scan diventerà uguale a scansionare l’intero file, cosa che io voglio evitare.

A questo punto opero attraverso la creazione di una sorta di albero: si crea il primo nodo che contiene un valore e due puntatori, uno che indica la pagina contenente le indicazioni dei valori minori di questo mentre l’altro indica la pagina che contiene quelli maggiori.

Bisogna però notare che adesso in questa sorta di albero ci sono due tipi di pagine diverse. Alcune che servono a aiutare la ricerca all’interno dell’*index-file* altre che servono a contenere il puntatore al *data-file*. Tutti questi valori che si puntano tra di loro vengono chiamati *nodi*: *foglia* se contengono il puntatore al *data-file*, *intermedi* altrimenti.

Adesso succede che si inseriscono altri dati, in modo che 2 pagine foglie non sono più abbastanza. Si procede quindi ad allargare i nodi intermedi, inserendo un altro valore, che indica la pagina contenente tutti i puntatore al *data-file* con i valori intermedi.

Di base questo permette di avere un inserimento nel *data-file* con un costo unitario, mentre inserimento nell’albero più costoso del valore unitario, ma sempre meno costoso di qualsiasi altra operazione. Di fatto quindi i *B+-tree* permettono di avere dei costi contenuti su quasi tutte le operazioni necessarie al database.

Questa struttura quindi si può semplificare in questo modo:

<img src="image/image-20220122193440979.png" alt="image-20220122193440979" style="zoom:50%;" />

Oppure si può pensare alla struttura creata in questo modo:

<img src="https://www-cdn.qwertee.io/media/uploads/btree.png" alt="PostgreSQL B-Tree Index Explained - PART 1" style="zoom:50%;" />





-- FINE LEZIONE 16, inizio lezione 17 --



Index file che ha una fila di puntatori alle pagine del datafile.

Ad eccezione della radice, ogni nodo ha un numero almeno metà del massimo.



## Unclustered B-tree

variante del unclu. è il

## Clustered Data-Tree

In questo albero, le foglie che partono da questo dato vanno tutte insieme. Quello che abbiamo è che i puntatori alla tupla sono raggruppati. 

Questi valori sono nella stessa pagina, se finisce lo spazio, altre tuple vicine sono raggruppate in un’altro spazio. Per questo motivo viene definito clustered.

Bisogna leggere il numero di pagine che contengono tutti le tuple cercate.



Modifica e eliminazione: praticamente uguale alla ricerca la ocmplessità ma con un costo aggiuntivo per la modifica



## Funzione di Hash

Funzione che restituisce un valore univoco. 



# Lezione 17 Nov.



Esecuzione delle join



#### Nested Loop Join

Questo valore viene calcolato con due loop nestati (doppio for).

Primo cicla sulle tuple e poi il secondo cicla per il controllo.

```java
foreach tupla t1 in R:
	foreach tupla t2 in S:
		if t1.A == t2.B:
			print t1, t2
```

Il costo di questa operazione è:

* per ogni riga => leggo tutte le tuple dell’altra tabella

$$
1 + |S| + \\
1 + |S| + \\
... \\
... \\
1 + |S| \\

\to |R| + |R|*|S|
$$

Questo risultato viene ottenuto perché si stanno leggendo le letture e le scritture. 

Ma noi sappiamo anche che ogni lettura equivale a una pagina, non ogni riga. Questo calcolo posso semplificare in questo modo:

* siccome devo leggere ogni pagina, allora posso fare in questo modo:

  * prendo la prima pagina di R
  * prendo la prima pagina di S
  * adesso posso leggere e scrivere con costo zero, e di fatto posso anche trovare tutte le tuple di *join*
  * poi posso seguire con la pagina seguente

* facendo in questo modo si ottengono i risultati => una volta che è a schermo si pensa come spazio libero in ram, siccome l’utente ha letto ormai.

* Questo calcolo quindi ha tempo di esecuzione: 
  $$
  P_r \times ( 1 + P_s \\
  1 + P_s \\
  .... \\ 
  1 + P_s ) \\ 
  $$
  Questo è il costo per fare la join, ovvero $P_r + P_r*P_s$.

* Siccome il costo dipende dalla lunghezza di una delle due, allora il DBMS prende la tabella con meno colonne in assoluto.

* Questo algoritmo funziona sempre, non cambia in base alla struttura del DBMS, in modo da essere sempre capace di fare le join. é molto probabile che si utilizzi questo modello se non si hanno degli indici particolari, o una variante ottimizzata.

* Per questo motivo l’operazione di Join è costosa: dipende dal prodotto delle due.



#### Sort-merge Join

Per migliorare l’algoritmo di prima, che rimane valido sempre in generale. Ma adesso possiamo ridurre un filo il tempo che impiega questa azione usando il fatto che: 

Se la tabella è ordinata non c’è nessun costo collegato al controllo che questa tabella sia ordinata o meno.

Se le tabelle non sono ordinate, allora prima paga il costo di mettere in ordine le tabelle in base ai due criteri cercati. Questo costo può non essere indifferente. 

Ora che sappiamo che i valori sono uguali, allora è come se guardassi le prime due tuple della tabella. Se una è più piccola dell’altra, allora non c’è modo di eseguire l’operazione di Join. Quando uno dei due valori è più grande dell’altro, allora mando avanti il più piccolo. Seguo questo controllo finchè non ottengo due tuple con lo stesso valore. (assumiamo che non si applichino duplicati).

Una volta che si trovano due valori uguali, ottengo la join tra i valori, poi continuo come se fossi appena partito eseguendo sempre il controllo di prima.

Se la tabella di partenza è finita, allora ho finito tutta la relazione.

Il costo di questa operazione è che ho letto tutte le tuple di $R$ una volta sola, e tutte le tuple di $S$ una volta sola. Se non ci sono dati riordinati allora bisogna aggiungere i dati di riordino.

Se ci sono duplicati poi  ??

Il costo quindi è questo: non c’è bisogno di tornare indietro su una pagina già letta, perché una volta controllato, allora so già per certo di averla già controllata.

Il tempo migliore dell’algoritmo per ordinamento è $N log_2(n)$. Ma questa relazione non vale perché è calcolata con tutto l’array in ram. Dopo puoi riscrivere le pagine su disco con un tempo lineare se riuscissi avere tutto il db in ram. Questo valore non è possibile, perchè non si può mai prendere il fatto che si possa avere tutto il db in ram. Logicamente questi algoritmi allora devono lavorare con meno dati possibili in ram e ottengono un risultato migliore possibile.

Se l’algoritmo di ordinamento ha (nel caso di postgres) a disposizione 3 pagine in ram (8kbyte *3). L’algoritmo di ordinamento è $2 * P_r * ( \ulcorner log_2(Pr) \urcorner + 1 )$. Questo algoritmo però lavora con un numero di pagine predefinite in ram, invece c’è un algoritmo che riesce a scalare il numero di pagine in ram. Se si riesce a fare questa operazione (scalare il numero di pagine in ram) allora si ottiene un valore più piccolo di tempo, sempre rimanendo lineare: $2 * P_r ( \ulcorner log_{B-1}(P_R/B) \urcorner +1)$. Questo valore è molto più piccolo siccome l’algoritmo lavora su un logaritmo molto più basso, quindi, rimanendo lineare, si ottiene un costo temporale minore. 

Per questo motivo si ha un sort-merge join. Assumendo che non si hanno chiavi uguali all’uguaglianza si ha allora:

*  il costo per ordinare $R$ (se si vogliono 3 pagine in ram oppure se si vogliono più di 3 pagine in ram)
*  il costo per fare la join con le tabella ordinate

Di fatto quindi si ottiene:
$$
2 * P_r ( \ulcorner log_{B-1}(P_R/B) \urcorner +1) + P_R + P_S
$$


Di solito quindi in questi casi, il DBMS cerca tra le strade le funzioni che costano di meno e poi svolge l’azione che costa meno.



#### Hash Join

Non ha a che fare con il fatto di avere un indice di hash sulle tabelle. 

Lo sforzo iniziale che dobbiamo fare per disporre i dati in modo più efficiente. Diamo per scontato che le relazioni siano salvate sul disco.

Usiamo una funzione di hash, che riceve un valore e restituisce un valore nel valore prefissato. Le funzioni di hash possono essere applicate a ogni tipo di dati. Una volta che applico la funzione allora è univoco il valore (se la funzione è fatta bene) e in questo modo è facile rendere le tuple uniche. 

A questo punto eseguo il modulo su questo valore e ottengo una tabella con questi valori, allora ho ridistribuito su disco secondo i *buckets*. Faccio questa operazione sia con $R$ che con $S$ con lo stesso numero di *buckets*. In questo modo la funzione butta nello stesso *buckets* sia i moduli con un valore di $R$ sia quelli di un valore di $S$.

In questo modo ho guadagnato che tutte le join da fare sono tutte nello stesso buckets. Questo perché si ha il valore di hash con un tipo di risposta uguale, allora questo valore può essere raggruppato.

Ci possono essere tuple che non hanno a fare nulla, ma so che se hanno da fare una join, allora sarà nello stesso buckets.

Lo scopo è che questa operazione separi tutte le operazioni in due pagine per buckets, in modo che posso caricarle entrambe in memoria e quindi svolgere tutte le operazioni a costo infinitesimo.



Una volta che ho tutti i dati nella hashtable. Alla peggio ogni tupla finisce in un buckets diverso. Per ognuna di queste tuple, allora devo leggere le rige che contiene e poi riscriverle su disco.

A questo punto ho fatto una lettura sulla pagina di $R$ e poi ho finito:
$$
(1 + 2 * ?_1 \\
1 + 2*?_2 \\
... \\
... \\
1 + 2 *?_m )

\to P_R + 2 * (?_1 + ?_2 + ... + ?_m) \\ 
\to P_R + 2*(|R|)
$$
Perché la cardinalità è il $?$: è il minimo tra cardinalità e numero di *buckets*. 



La cosa migliore è che il buckets riesca a ottenere tutte le tuple in un buckets solo. Se non si riesce allora si ottiene un valore un filo più grande, e questo valore è quello necessario per ottenere tutte le tuple di $R$, 







```mysql
SELECT *
FROM R
WHERE R.name LIKE 'c%'
```

Utilizzare il B+tree, e non posso usare l’hash perché ho bisogno di tutta la parola.

```mysql
SELECT COUNT(*)
FROM R
WHERE R.age < 30;
```



# Lezione Database 22 Novembre

Il database quando deve fare un operazione la scompone e calcola il costo dell’operazione totale.



## Costo per le operazioni

$ R(A,\bar B) \Join S(\bar B, C)$ => che ha costo di  $P_R + |R| * (L_{B+1})$.

La proiezione con il costo $1+L_B + 1$. Ma questo costo io non la ho, siccome questa relazione viene costruita un pezzo alla volta. I valori in uscita dall’operazione di $\Join$ vengono mandati al’operazione di proiezione.

Ogni volta che viene eseguita l’operazione quindi si applicano entrambe, senza avere necessità di rileggere le pagine. Questa operazione ha costo zero.

L’operazione dopo è una $\Join$, ma l’unico modo per avere questa operazione è un prodotto cartesiano, siccome si utilizzano dei dati che non hanno nulla in comune.

Tutto il resto quindi sono operazioni che possono essere fatte al volo con una singola lettura. Di fatto quindi tutta questa operazione viene eseguita in un tempo di $



Di solito il problema dei DBMS è che le combinazione di applicazione di questi operatori sono potenzialmente infinite. Di fatto loro non provano tutte le possibili, ma solo un sottoinsieme che secondo loro sono i migliori.





```sqlite
SELECT *
FROM Train
WHERE prodotti LIKE '%7085%'
```

Ricerca di un prodotto nel quale compare un prodotto con un valore al suo interno di $\%7085\%$.

I tempi sono uguali sia che sia un B+Tree, siccome l’unico modo per questa ricerca è uno scan completo. 

In questo modo non si può fare altro che passare tutti i valori disponibili. Questo avviene perché non ho un prefisso chiaro sul prodotto, ma se solo avessi un valore iniziale allora basterebbe una ricerca attraverso un albero.

 

