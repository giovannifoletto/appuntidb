# Dipendenze Funzionali e Forme Normali

Risolvere problemi di diagrammi ER sbagliati o non completamente corretti.

Un altro tipo di problema in questo modo è che siamo costretti a raggiungere un professore in caso di vincolo di diagramma. 

Questi problemi sono molto comuni, siccome non sempre ci si mette a tavolino prima a organizzare il db. 

Il problema più frequente è la **ridondanza dei dati**, ovvero che ci sono valori nelle tabella dalla quale si può rivelare il contenuto di altri. Di fatto quindi non serve scrivere le informazioni che vengono recuperate, siccome il primo valore identifica automaticamente il palazzo e il suo valore.

Esempio: Fisica ha una serie di valori di `Palazzo` e `Budget`, che non serve indicare tutte le volte, ma che il valore stesso di `Fisica` indica già.

Questi valori si dicono **dipendenti funzionali** siccome il valore di `Palazzo` e il `Budget` sono dipendenti dal valore `Fisica`. 

La dipendenza funzionale è qualcosa che ha a che fare con il contenuto della tabella o qualcosa di più? No i vincoli non sono legati ai dati, ma **allo schema**, alla relazione. Questo problema è proprio dello schema e della relazione che si sta cercando di risolvere. 

Di fatto vedere la tabella può servire per indicare un problema nella tabella, ma questo non permette di concludere che ci sia una dipendenza funzionale. Di fatto è necessario sapere lo schema logico per sapere questa informazione, perché magari è solo un caso.

Se io non conosco la semantica della mia applicazione non posso capire guardando solo i dati che c’è una dipendenza funzionale, ma è necessario conoscere lo schema logico.

Scoprire una dipendenza funzionale permette di ridurre al massimo la ridondanza, in modo da avere meno lavoro per mantenere la tabella valida.

ci sono un sacco di vincoli sulla tabella (che se si vuole è una sorta di dipendenza funzionale), la chiave è un particolare tipo di dipendenza, infatti una relazione permette di determinare un sottoinsieme degli attributi. La chiave invece indica univocamente di una particolare tupla, con non tutti i valori uguali.

Di fatto quindi l’unico modo per sapere sicuramente se si ha una dipendenza lineare è attraverso lo studio della relazione.

Le istanze che soddisfano questo vincolo sono legali.

Se queste dipendenze esistono, allora vuol dire che c’è ridondanza di dati. A questo punto allora si può raccogliere tutto in un insieme `F`. La relazione con i suoi attributi allora si possono inserire tutte le relazioni funzionali nel sottogruppo. `<R(U), F>`.

Come si legge $X \to Y$: $X$ determina funzionalmente $Y$. Non ha il significato di implicazione logica.

Si supponga `<R(ID, name, salary, dept_name, building, budget)>`: 

* `ID` determina tutti gli altri attributi, quindi si dice `ID` $\to$ `name, salary, dept_name, building`.
* Ci possono essere delle chiavi non primarie (come si chiamano) e questo schema indica il problema e si può risolvere.

Una volta che si sono trovate delle dipendenze funzionali con zone di ridondanza, allora si esegue una **decomposizione**, ovvero un operazione per puntare a ridurre questi dati.

Per fare la decomposizione, si spezza la tabella in due minori con lo scopo di avere diviso le infomazioni duplicate, poi eseguo una Join. Questo ottengo quindi un valore di un indirizzo che ha due valori diversi sbagliati insieme. 

Questa decomposizione si chiama **lossy**, ovvero una **decomposizione con perdita di dati**. Di fatto si ottiene una tabella con più tuple rispetto a quelle di partenza (con dei dati sbagliati anche in caso) oppure si perdono dei dati. Questa operazione fa più danni di quelli che risolve, si deve a tutti i modi evitare.

Le decomposizioni che vogliamo si chiamano **lossless**, ovvero senza perdita. Una decomposizione si ottiene con:

* identifico i valori duplicati
* le relazioni ottenute e la decomposizione in più attributi e li ricompongo, quello che ottengo sono gli attributi di quello di prima.
* Questa decomposizione è senza perdita se, quando prendo la tabella originale e la ricompongo e li proietto sugli attributi della prima e metto in join fino all’ultima parte, il risultato deve essere esattamente quello che avevo prima.

Questa proprietà deve valere per tutte le istanze della relazione, qualsiasi sia il contenuto della base di dati. Questa proprietà quindi è ancora una volta una proprietà dello schema, non della base di dati.

Per ogni istanza quindi, ottenendo tutte i valori ottenuti in questa interfaccia devo avere tutti i valori di prima, nessuno di più, nessuno di meno.

LA decomposizione è senza perdita se funziona **sempre** e non dipende dal contenuto.



Il nostro obiettivo per migliorare una base di dati:

1. trovare i dati con dipendenze funzionali (certezza assoluta solo con l’analisi dello schema)
2. una volta trovate allora dobbiamo trovare le dipendenze che non rappresentano chiavi
3. serve un metodo valido che decomponga la relazione, in modo che questa sia senza perdita (se è con perdita allora non serve a nulla)
4. l’obiettivo è: 
   * ridurre le ridondanze senza perdere dati
   * le relazioni valgono lo stesso, ma senza perdere le proprietà del nostro sistema. Se queste si perdono, allora è possibile che si faccia molta confusione con i dati inseriti nel db.



Esistono dei casi anche in cui si può preferire mantenere queste tabella piene di dipendenze funzionali.

Per ottenere una decomposizione senza perdita sono necessarie le **forme normali**.

Ci sono delle ridondanze definibili come critiche, partendo dal trovare le dipendenze funzionale.

Si trova un insieme di dipendenze funzionali chiamato `F`. Spesso si ottengono alcuni valori dipendenti impliciti, con un valore che. (MANCA)



Queste regole permettono poi di avere un database:

* **sound**  (corretto), tutte le dipendeze funzionali sono valide
* **complete**: queste 3 regole svolte fino a saturazione allora non si è perso nessun dato