# Corso di Database

> AA 2021
>
> Prof. Marco Calautti



# Index

[TOC]

# Introduction

Un database è un insieme di dati interconnessi, ovvero una collezione integrata di dati (dove tra i vari dati sussistono delle relazioni specifiche).

In questo database i dati sono chiamati entità e sono collegati tra di loro attraverso delle relazioni.

Per gestire il database (ovvero l’insieme dei dati) si può utilizzare il DBMS (DataBase Management System), ovvero un software o un insieme di software che serve a gestire i dati.



## Il Problema

è necessario avere un insieme di dati raccolti dal programma. A questo punto voglio raccogliere tutti i dati in dei file di testo. Questo approccio al problema creerebbe un sacco di lavoro per lo sviluppatore che si troverebbe a dover creare un applicazione che, oltre ai compiti normali, avrebbe da gestire anche:

* gestione della memoria centrale e della secondaria
* buffering, page-oriented access, 32-bit addressing (ovvero il fatto che nei sistemi a 32 bit è necessario avere degli indirizzi di memoria per ogni slot di 2GB di dati).
* il codice necessario a fare ricerche sui dati
* inconsistenza dei dati nei file e concurrent-users (ovvero la concorrenza degli utenti)
* crash recovery (se l’applicazione crasha, come recupero i dati)
* security and access control (chi, quando e dove si possono ottenere i dati)

Per tutti questi problemi appunto si utilizza il DBMS che permette di avere queste funzioni già completamente applicate all’insieme di dati. In questo modo si ha un metodo per interrogare questo “software” in modo molto veloce e ottenere i dati cercati.

I DBMS permettono quindi di avere una serie di lati positivi:

* gestisce il data-indipendence
* gestisce efficacemente gli accessi ai dati
* riduce il tempo necessario per lo sviluppo di un applicazione
* risolve il problema del data-integrity (ovvero della consistenza dei dati) e della sicurezza dei dati
* gestisce in modo uniforme l’amministrazione dei dati
* gestisce gli accessi concorrenti e il crash-recover.

La struttura del DBMS è fatta (ad alto livello) in questo modo:

<img src="image/image-20220103234355331.png" alt="image-20220103234355331" style="zoom:67%;" />

Come si può anche capire quindi questa applicazione non è sviluppabile in poco tempo e ha necessità particolari anche spesso non semplici da implementare. Per questo motivo e per altri particolarmente importanti il DBMS è la scelta con cui andare per avere un modello di immagazzinamento dati efficace.



## Data Model

Per interfacciarci con questi dati in modo di avere la **data-indipendence**, ovvero un modello che permetta di accedere ai dati nell’insieme senza dover cercare in ogni sottofile.

Per effettuare questo accesso si utilizza il **modello dati** (o data model). Il modello più utilizzato è il **modello relazionale** utilizzato in tutti i DBMS. Questo modello si basa sulle tabelle o relazioni.

In queste tabelle si avranno:

* le colonne che indicano le caratteristiche di ogni oggetto
* ogni oggetto è rappresentato dai dati di una riga

Quando vogliamo creare una relazione (o tabella) è necessario creare lo *schema* della tabella della relazione, ovvero la descrizione di un insieme di dati.



Un modello di dati, in particolare il modello relazionale, è importante siccome permette di avere un livello di astrazione tra i dati e l’applicazione. Attraverso questo modello infatti descriviamo il modello logico dei nostri dati, ovvero le caratteristiche di ogni dato e le connessioni con altri dati.

Tutte le informazioni che riguardano poi la memorizzazione dei dati sul disco poi viene gestita più a basso livello dal DBMS, mentre noi otteniamo un modo per accedere, modificare o visualizzare i dati ad alto livello.

Ad esempio: abbiamo un modello relazionale per avere un database universitario del personale:

```sql
Students(sid:string, name:string, login:string, age:integer, pga:real);
Coursed(cid:string, cname:string, credits:integer);
Enrolled(sid:string, cid:string, grade:string);
```

Queste descrizioni nel modello logico indica semplicemente come sono costruiti i dati salvati sul disco.

Il gap tra funzionamento ad alto livello e applicazione pratica di questo modello di dati viene svolto dal **modello fisico** che traduce in azioni pratiche le viste dei dati disponibili in base a queste relazioni.

Nel modello di dati si può avere un ulteriore livello di astrazione, che sono le viste (*view*). Di fatto queste sono delle relazioni a tutti gli effetti, ma con dei vincoli o dei modelli particolari. Queste *view* si possono ottenere dei valori nuovi, utilizzando i dati presenti nel DBMS. Solitamente queste relazioni sono definite dallo sviluppatore e permette di avere una serie di informazioni aggiuntive sul modello di dati. Inoltre di solito si ottengono i risultati di queste viste da un calcolo sul momento, non si hanno i dati salvati sempre sul disco all’interno di queste viste logicamente.

Inoltre si possono avere delle viste particolari per limitare l’accesso ai dati disponibile all’utente finale.



Ottenuti questi concetti si può riprendere il concetto di *data-indipendence*: anche se si cambia il modello logico, l’utilizzatore finale che si interfaccia con le viste non ottiene nessun cambiamento dell’applicazione. Questa indipendenza dei dati in particolare si chiama *logical-data indipendence*.

Il modello logico invece permette di avere un indipendenza sul modello fisico, infatti anche se si cambia il modello di salvare i dati o di interrogarli, il tipo di dati e il modo in cui si relazionano tra di loro rimane costante. Questo concetto si chiama *physical-data indipendence*.



## Accesso Concorrenziale ai Dati

Il DBMS permette a più utenti di modificare i dati in modo concorrente. In questo modo possiamo fare accessi molto veloci ai dati, nonostante si abbiano degli utenti che richiedano degli accessi molto più complicati di altri.

Il DBMS permette di avere degli utente di lavorare in parallelo, utilizzato i “tempi morti” dei processi tra gli utenti.

Questo però crea un problema, infatti se due utenti lavorassero sugli stessi dati allora si avrebbe un inconsistenza. Il DBMS può gestire anche questo problema, rendendo l’esecuzione delle operazioni sequenziali.

Per gestire l’accesso concorrente il DBMS utilizza il concetto di transazione: ogni sequenza di azioni è atomica, quindi può essere o completamente svolta o non svolta per nulla. Di fatto il DBMS non può garantire che l’esecuzione poi sia senza errori, però garantisce di eseguire un operazione alla volta.

Di fatto posso avere dei vincoli di integrità dei dati che permettono al DBMS di capire si svolgerà un errore e eseguire quindi un *abort*, svolgendo tutte le azioni all’incontrario per evitare dei danni ai dati, avendo sempre un modello di dati **consistente**.

Di fatto utilizza un modello di garanzia che permette di avere assicurato il funzionamento di una transazione (un operazione nel dbms) attraverso una serie di operazioni singolarmente valide. Se ognuna di queste operazione è valida allora si inizia a svolgere l’azione, oppure annulla del tutto l’operazione.

Per garantire questo modello il DBMS utilizza un log, ovvero un file che memorizza ogni singola operazione svolta dal DBMS. Questa operazione che permette di scrivere nel log prima di svolgere l’operazione allora si chiama **write-ahead log protocol**, ovvero quando si scrivono dei log delle operazioni prima che queste vengano di fatto svolte.

Questo modello permette quindi di capire sempre in che stato è il modello di dati. Infatti se si ottiene un crash del sistema (dovuto a un errore) allora il DBMS è capace di ripristinare lo stato dei dati consistente prima che questa azione venisse svolta, rileggendo e eseguendo all’indietro queste operazioni.



# Il modello entità-relazionale (ER)

Il modello entità-relazione è un linguaggio grafico che permette di descrivere la struttura del modello relazionale del nostro database, in modo da semplificare gli attributi di ogni entità e le relazioni che si creano tra diverse entità.

## Entità

In questo modello si identificano le entità, ovvero un oggetto unico distinguibile all’interno del database, in questo modo:

![](image/image-20220104221740426.png)

Le entità simili poi si raggruppano in **entity-sets**, che quindi avranno:

* stesso set di attributi
* una chiave (**key**)
* un dominio, o tipo di dato (**domain**)



## Relazioni

Queste non sono le tabelle del modello relazionale (infatti in inglese questo termine è **relationships**), ma invece sono il modo in cui due o più entità si connettono tra di loro.

Le relazioni nel modello ER, si rappresenta con:

![image-20220104222321648](image/image-20220104222321648.png)

Solitamente il nome di questo campo rappresenta il rapporto tra le due entità che si stanno collegando.

Da queste relazioni si ottengono le **relationship sets**, ovvero le tabelle che rappresentano il “valore” della relazione sotto forma di tabella:

* ogni colonna rappresenta un’entità facente parte della relazione
* ogni riga rappresenta i valori, provenienti da ogni set, che sono connessi dalla relazione

Questo modello non significa che tutti gli oggetti di un entità siano collegati a tutti gli oggetti dell’altra.

Ad esempio una relazione “starts-in” tra “movies” e “film” può essere rappresentata in questo modo:

| Movies         | Stars                 |
| -------------- | --------------------- |
| Basic Instinct | Sharon Stone          |
| Total Recall   | Arnold Schwarzenegger |
| Total Recall   | Sharon Stone          |
| ..             | ..                    |



Ci possono essere più tipi di relazioni:

* many-to-many
* many-to-one
* one-to-one

Rispettivamente, utilizzando l’esempio di prima, si ottengono questi modelli di relazione:

<img src="image/image-20220104223110187.png" alt="image-20220104223110187" style="zoom: 50%;" />

Le relazioni più comuni sono quelle binarie, ovvero le relazioni che mettono in comunicazione due entità. Questo non toglie che, in alcuni casi, è necessario avere una relazione a più vie (di solito ternaria è al limite).



## Relazione a più vie (Relazione ternaria)

Questo tipo di relazione riesce a collegare più di due entità attraverso una relazione. In questo modo si otterrà una tabella rappresentante questa relazione così costruita:

| Stars                 | Movies         | Studios  |
| --------------------- | -------------- | -------- |
| Sharon Stone          | Basic Instinct | Sony     |
| Arnold Schwarzenegger | Total Recall   | Columbia |
| Sharon Stone          | Total Recall   | Columbia |
| ..                    | ..             | ..       |



Le relazioni a più vie sono molto utili per unire diverse entità, ma allo stesso modo creano dei pesanti problemi di gestione dei dati, infatti si ottengono dei problemi della gestione delle connessione tra le varie entità.



### Convertire in una relazione a tre vie in una a due vie

Questa operazione si può fare ma si devono capire le relazioni in modo particolare. In questo caso si può ottenere una relazione di questo tipo attraverso un diagramma così creato:

<img src="image/image-20220104225636799.png" alt="image-20220104225636799" style="zoom:50%;" />



## Ruoli nelle Relazioni

Le relazioni possono avvenire anche all’interno dello stesso entity-set. In questo caso però è necessario indicare il ruolo che queste entità presentano, siccome la tabella di riferimento non può avere due colonne con lo stesso nome.

Questo tipo di relazione può essere *simmetrica*, ovvero i dati presenti sulla tabella possono essere ripetuti più di una volta. Ad esempio una relazione che inserisce le coppie sul set:

* se la relazione viene definita come *moglie* e *marito* allora si creerà una sola relazione ogni attore

* se la relazione viene definita come *attore1* e *attore2* allora si otterrà una doppia relazione siccome:

  | Attore1       | Attore 2      |
  | ------------- | ------------- |
  | Tom Cruise    | Nicole Kidman |
  | Nicole Kidman | Tom Cruise    |

In tutti i casi questa indicazione può essere specificata nel diagramma ER, ma non può essere applicata direttamente agli attributi della relazione in pratica, infatti l’unico modo per fare in modo che questa relazione non sia simmetrica è attraverso l’interrogazione e il filtro di un applicazione esterna (che non è il DBMS).

Per il resto questo tipo di relazione è esattamente uguale alla *two-way relationships*.



## Attributi della Relazione

In qualche caso si possono avere degli attributi che non appartengono completamente a nessuna delle due entità facente parte della relazione, ma che è invece propria della relazione in se.

Ad esempio:

<img src="image/image-20220104225039216.png" alt="image-20220104225039216" style="zoom:50%;" />



Questa relazione quindi avrà un certo attributo che dipende dalle due entità messi in relazione, e verrà rappresentata in questo modo:

| Stars                 | Movies         | Salary  |
| --------------------- | -------------- | ------- |
| Sharon Stone          | Basic Instinct | 100.000 |
| Arnold Schwarzenegger | Total Recall   | 100.000 |
| ...                   | ...            | ...     |



## Aggregazioni

Nel modello ER si possono unire delle relazioni in modo da mettere l’intera tabella in relazione con altre entità. Questo modello permette di utilizzare dei *relationship set* come un semplice *entity set* in modo da renderle partecipanti in altre *relationship*.

![image-20220104233759117](image/image-20220104233759117.png)

(Logicamente in questo schema ogni entità sarà definita insieme ai suoi attributi. Inoltre la relazione in se può avere anche lei attributi)



## Sottoclassi

Nel nostro diagramma ER si possono trovare delle entità che sono solo dei casi speciali di un altra entità. Questo significa che si otterranno delle entità che hanno degli attributi in parte derivati dalla “classe” originale e altri che sono propri della “sottoclasse”.

Questo modello si rappresenta così:

<img src="image/image-20220104234325957.png" alt="image-20220104234325957" style="zoom:67%;" />

Le chiavi in questa relazione viene ereditata dagli attributi della super-classe.



## Chiavi e Chiavi Candidate

La chiave in ogni *entity-set* è un attributo che deve esistere in modo univoco in tutto il dataset.

Di fatto però questa nozione di chiave è molto libera, infatti aggiungendo all’insieme degli attributi un oggetto definito come unico in tutto il dataset, allora tutto l’insieme di attributi può diventare chiave.

Per questo motivo si parla di **chiave candidata**, ovvero un insieme minimale di chiavi univoche. Tra questo insieme di chiavi univoche si può scegliere un unica chiave, che diventerà **chiave primaria**. Questa scelta è fatta seguendo una logica di convenienza, comodità o di scelta semplicemente. Questa chiave primaria diventa quindi quella rappresentativa di ogni elemento dell’entity-set.



## Entità deboli (weak entity sets)

Fino ad ora abbiamo avuto tutte le entità strettamente rappresentate con un insieme di attribuiti, di cui uno si è scelto tra gli altri come univoco e quindi come chiave primaria.

Ci sono però dei casi in cui si ha un entity set che ha necessità di una relazione così forte, che la chiave primaria non è presente sulla propria entità, ma dipende quindi da una relazione. Queste entità dipendono quindi dall’esistenza di un altra entità, per questo vengono definite **entità deboli**.

Nei diagrammi ER questo tipo di relazione viene rappresentato da:

<img src="image/image-20220105000023496.png" alt="image-20220105000023496" style="zoom:50%;" />

(si usano necessariamente la linea piena con la freccia, perché c’è la necessità di fare un collegamento 1:1)

Di fatto questo schema evidenzia che l’entità debole non può esistere senza l’esistenza dell’entità.

Poi nell’applicazione vedremo anche che per definire questo tipo di dati nella nostra base di dati si avrà un impostazione che definirà il modo in cui si trattano i dati dipendenti rispetto all’eliminazione delle entità forti in relazione.



Le relazioni deboli possono essere anche messe sotto forma di *chain of weakness*, ovvero una serie di relazioni deboli. Questo modello può essere rappresentato in diagramma ER con questo schema:

<img src="image/image-20220105000442218.png" alt="image-20220105000442218" style="zoom:50%;" />

In questo modello di dati quindi, l’eliminazione dell’entità non debole produrrà la cancellazione a cascata delle altre due entità, che di fatto non hanno senso di esistere “a catena”.



# Il modello relazionale

Il database relazione è un insieme di relazioni. Di fatto le relazioni sono tabelle alle quali si aggiungono informazioni per indicare i modi in cui i dati si interfacciano tra loro (queste informazioni aggiuntive infatti vengono chiamate relazioni).

Istanza della relazione è la tabella, descritta da righe (o cardinalità) e colonne (o arità).

La tabella viene definita attraverso uno *schema*, che indica gli attributi della relazione e il loro tipo di dato.

Di fatto in queste tabelle avremo un insieme di righe immutabili (chiamate infatti tuple) che sono una serie di oggetti con determinati attributi.

Un esempio di istanza può essere:

| sid   | name  | login      | age  | gpa  |
| ----- | ----- | ---------- | ---- | ---- |
| 53666 | Jones | jones@cs   | 18   | 3.4  |
| 53688 | Smith | smith@eecs | 18   | 3.2  |
| 53650 | Smith | smith@math | 19   | 3.8  |

Questa istanza ha:

* cardinalità = 3
* grado/arità = 5
* tutte le righe sono elementi distinti (nessun elemento è ripetuto). Infatti nel modello matematicamente relazionale si assume sempre che ogni oggetto sia unico. Di fatto nella pratica invece questo vincolo viene spesso rilassato per favorire le performances.
  * Questo non influisce sulla possibilità di avere dei dati al loro interno uguali (ad esempio si noti `age` che ha valore `18` in due diverse tuple presenti nella tabelle)
* lo schema di questa tabella può essere `Students(sid:int, name:string, login:string, age:int, gpa:real)`



Il linguaggio con cui si definiscono queste tabelle è chiamato **SQL (Structured Query Language)** e è stato creato nel 1970 da IBM. Questo linguaggio è divenuto poi uno standard in modo da favorire il funzionamento di questi comandi su qualsiasi tipo di database esistente. SQL poi si è sviluppato in diverse release successive al 1970.

SQL ha molte parti di linguaggio che servono a creare e amministrare il database in ogni sua parte. La prima sezione di questo linguaggio è chiamato **SQL: DDL (Data Definition Language)**.



## SQL: DDL (Data Definition Language)

Questo parte del linguaggio SQL permette di definire la struttura della base di dati, non permette di recuperare nessun tipo di dato dalla tabella.

In questa sezione del linguaggio si avranno diversi comandi:

* creare una tabella:

```sql
CREATE TABLE 	Students( sid: CHAR(20),
						name: CHAR(20),
                      	login: CHAR(10),
                        age: INTEGER,
                      	gpa: REAL)
```

SQL ha un infinità di tipi di dati, che possono adeguarsi a tutti i valori che dovranno contenere. La stringa in questo caso viene definita come un array di 20 caratteri. Questa indicazione viene interpretata dal DBMS in modo definito, quindi anche se il valore è minore di 20 caratteri, riempe di spazi se non si arriva a questo valore. Per togliere questo constrains si usa `VARCHAR`.

* eliminare la tabella

  ```sql
  DROP TABLE Students
  ```

  Questo comando svuota la tabella e la elimina completamente anche la relazione (compreso lo schema, quindi tutte le relazioni al suo interno).

* cambiare una tabella:

  ```sql
  ALTER TABLE Students
  	ADD COLUMN firstYear: integer
  ```

  Questo comando impone la creazione di un’altra colonna su una tabella già esistente. Il problema derivato da questa operazione è che non si che dati inserire in questo attributo per le tuple già esistenti nella tabella. Di default il DBMS inserisce `NULL` al posto del campo che non è definito. Questo è un problema perchè in SQL il `NULL`è indefinito, quindi la risposta in caso di confronto è indefinito, e in base al DBMS viene trattato come vero o come falso. Questo significa che avremo un comportamento no coerente sull’esecuzione di un comando riguardo questo attributo.



## Vincoli di integrità

In ogni tabella, in fase di creazione, si possono definire una serie di regole (**Integrity Constrains (ICs)**) che validino i dati all’interno della tabella. In questo modo la tabella è definita sempre valida per una certa relazione se tutti i vincoli definiti sono rispettati.

Inoltre attraverso il controllo di questi vincoli si può ottenere un modello di dati consistente, infatti tutte le tuple che non soddisfano questi vincoli verranno bocciate dal DBMS e non sarà possibile recuperarle.

* *primary key constrains*: chiave primaria, non ci possono essere 2 righe con lo stesso id di chiave. La chiave è candidata primaria perchè se la togli non riesci a distinguere i valori, mentre è primaria perchè è la principale.

* *primary and candidates keys in SQL*:

  ```sql
  CREATE TABLE Enrolled(
  		sid CHAR(20),
  		cid CHAR(20),
  		grade CHAR(2),
  		PRIMARY KEY(sid, cid) )
  ```

* costrutto `UNIQUE`:

  ```sql
  CREATE TABLE Enrolled(
  	sid CHAR(20),
  	cid CHAR(20),
  	grade CHAR(2),
  	PRIMARY KEY(sid),
  	UNIQUE(cid, grade))
  ```

  Gli attributi che non possono essere mai nulli, perchè servono per identificare gli elementi. In questo modo l’unica colonna che *deve* assolutamente avere un valore è sid (può partecipare a un solo corso e avere una sola matricola => non posso avere due voti), mentre gli altri valori che sono *unique* ma anche che lo studente non può avere lo stesso voto in due corsi diversi (senza senso). Inoltre non si possono avere due studenti che hanno lo stesso voto nello stesso corso.

  *Usando i vincoli in modo sbagliato, potremmo impedire alla nostra base dati di contenere dei dati che invece dovrebbero/potrebbero contenere*

* *Foreign Keys*: il vincolo chiave esterna mi indica usata per indicare una coppia in un altra relazione (deve corrispondere alla chiave primaria della seconda relazione).

  es:

  ```sql
  CREATE TABLE Enrolled
  (esid CHAR(20), cid CHAR(20), grade CHAR(2),
   PRIMARY KEY(esid, cid),
   FOREIGN KEY(esid, cid) REFERENCES Students)
  ```

  C’è il vincolo di chiave esterna, in cui indichiamo che colonna è interessata, che fa riferimento alla tabella studenti. Sa che valore dare a questo attributo, perchè se deve fare riferimento a studenti, stiamo implicitamente dicendo che deve fare riferimento alla chiave primaria della tabella studenti. (non è necessario aggiungere nulla, siccome c’è la chiave primaria).

  Questo vincolo non dice che non ci possono essere studenti non iscritti ai corsi, ma solo che un iscritto al corso sia presente negli studenti.

  Come fa il DBMS a gestire questi controlli? Se inserisco un iscritto con la matricola di uno studente non esistente. Se si aggiunge un dato sbagliato, il DBMS blocca l’aggiunta del dato, non facendo fare l’operazione.

  Al contrario come faccio a prevenire una modifica dei dati già salvati sul database, aggiungendo un dato che non risponde più ai vincoli. In questo caso il DBMS ha più di un azione possibile:

  * `NO ACTION`: non faccio nulla, impedisco l’operazione e basta
  * `CASCADE`: a cascata eliminare tutte le cose che contengono le sue informazioni (o aggiornare tutte queste informazioni)
  * `SET NULL/SET DEFAULT`: sostituisco il valore con un valore di default (che devo definire) o nullo (se mi voglio far male)
  * se non si scrive nulla si assume `NO ACTION`

  Queste azioni sono attuabili attraverso questi dei comandi particolari:

  ```sql
  NO ACTION
  CASCADE
  SET NULL/SET DEFAULT

  CREATE TABLE Enrolled(
  	sid CHAR(20) DEFAULT 0,
  	cid CHAR(20),
  	grade CHAR(2),
    PRIMARY KEY(sid, cid),
    FOREIGN KEY(sid),
    	REFERENCES Students
    		ON DELETE CASCADE -- si definiscono le modalità di risposta all'eliminazione di uno studente
      					-- questo comando indica l'eliminazione di tutti gli elementi che fanno riferimento al dato eliminato nella tabella
    		ON UPDATE SET DEFAULT	-- questo comando invece indica il valore di default in caso di cambiamento di una tabella
  )
  ```



I vincoli che imponiamo sul database sono di solito settati basandoci sul progetto ER, in modo da rappresentare il comportamento del DBMS più vicino alla realtà possibile.



## Passare dall’ER al modello relazionale

In questa sezione vedremo come passare da una descrizione del modello attraverso schema ER, a una mappatura con un modello relazionale completo.

La cosa più facile è la creazione delle entità, infatti è necessario solo operare un `CREATE TABLE`. Un esempio di questa operazione può essere:

```sql
CREATE TABLE Employees (
	ssn CHAR(11),
    name VARCHAR(20),
    lot INTEGER,
    PRIMARY KEY(ssn) -- Questa indicazione è importante perché permette di capire come gli oggetti siano unici all'interno del dataset
);
```

Vediamo invece come mappare in modello relazionale una relazione tra due entità.
Il diagramma ER che cerchiamo di creare nel nostro modello relazionale è il seguente:
