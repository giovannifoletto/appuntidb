# Database

Il problema che puntano a risolvere i database è quello della data indipendence, ovvero come memorizzare i dati sul disco in modo uniforme e in modo univoco, attraverso un modello relazionale (ovvero *tabelle*). 

In queste tabelle ogni riga è un oggetto, e ogni colonna è un entità e questi valori sono in relazioni con se stessi per organizzare le entità in modo univoco. 



Le *entità* sono gli elementi del nostro sistema di cui vogliamo modellare. Queste entità hanno degli **attributi**, ovvero delle caratteristiche per descrivere questa entità. L’insieme di queste due cose crea l’**entity set**. Di solito tra questo insieme di valori c’è anche un modo per identificare univocamente l’oggetto descritto in modo che sia impossibile avere lo stesso oggetto più di una volta nella stessa base di dati.

Ogni attributo ha un dominio, ovvero il tipo di dato che viene contenuto in quello slot.



## Costrutti relazionari

Il costrutto di relazione (**relationship**) è rappresentato dal rombo:

![image-20210922112604260](./image/image-20210922112604260.png)



Ma ci possono essere più tipi di relazione tra degli identity-set:

### Many-to-Many

Questa relazione impone che tutti gli elementi possano collegarsi a più di un entity, e viceversa.

Usando l’esempio di prima, è come dire che: un film può avere più di un attore e più attori possono partecipare a più di un film.

Il simbolo è (nel modello esplicito):

![image-20210922113059873](C:\Users\giova\AppData\Roaming\Typora\typora-user-images\image-20210922113059873.png)

Dove l’indicazione `0:N` indica che le relazioni di questo oggetto può essere da `0` fino a `N`. Le  



### Many-to-One

Questa proprietà indica che molte entità sulla sinistra possono essere associate e un unico valore sulla destra.

Questa relazione è indicata con il simbolo:

![image-20210922114852718](C:\Users\giova\AppData\Roaming\Typora\typora-user-images\image-20210922114852718.png)

Ad esempio, se avessimo la relazione tra *employers* e *departements*.

![image-20210922115331685](C:\Users\giova\AppData\Roaming\Typora\typora-user-images\image-20210922115331685.png)

Questo schema dice che un impiegato può avere al massimo un dipartimento, ma un dipartimento può avere più di un impiegato.



### One-to-One

Questa relazione indica che ogni entity può essere in relazione con uno e uno sola entity dall’altra parte.

Questo simbolo si ottiene con il simbolo: 

![](C:\Users\giova\AppData\Roaming\Typora\typora-user-images\image-20210922115617367.png)

Esempio: manager e dipartimenti devono essere strettamente e obbligatoriamente collegati in questo modo, siccome un manager non può amministrare più di un dipartimento, e ogni dipartimento può avere al massimo un manager:

![image-20210922115759454](C:\Users\giova\AppData\Roaming\Typora\typora-user-images\image-20210922115759454.png)



Queste relazioni servono a indicare meglio le relazioni nella nostra base di dati, e possono essere accompagnate anche da altre informazioni, come ad esempio:

* **at most one**: relazione che impone la relazione o nulla o con una sola altra entity.

  ![image-20210922120129699](image/image-20210922120129699.png)

* **at least one**: relazione che impone che la relazione non sia mai nulla, ma al minimo sia con un solo elemento

  ![image-20210922120251461](image/image-20210922120251461.png)

* **exactly one**: impone la relazione uno a uno con gli elementi

  



