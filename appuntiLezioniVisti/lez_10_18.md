# 18 Oct. 2021



## SQL statement

```sql
select 
	from -- Si chiamano clausole, indicano da dove prendere i dati
	where -- con che modalità prendere i dati
```

Risultato è una relazione con gli attributi messi in input.



$\pi_{name, income} (\sigma _{age< 30} (person))$ Questa query in algebra, si traduce in:

```sql
select person.name, person.income
from person
where person.age < 30
```

Questa operazione è che prende una tabella e inizia a scansionare le righe, poi la where indica le clausole per tenere o buttare una riga.



Notiamo che l’attributo viene inserito con il nome della tabella prima : `tabelName.objectName`. Se non ci sono ambiguità allora si può evitare tutto. 

Posso anche rinominare degli oggetti per l’interrogazione:

```sql
select p.name as name, p.income as income
from person as p -- rename persone as p
where p.age < 30
```

Inoltre il `p.name as name` è un modo per cambiare il nome delle colonne.



Il risultato di operazione per SQL non rimuove i duplicati, perché è troppo costosa dal punto di vista computazionale. Per avere un elenco di elementi unici, allora devo:

```sql
select surname, city from employees -- not remove duplicates

select distinct surname, city from employees -- removes duplicates
```



Tutti altri operatori sono poi definiti come un normale linguaggio di programmazione:

```sql
select name, age, income
	income as repeatedIncome -- sto aggiungendo una 4ta tabella con la copia dello stipendio
from person
where income >= 20 and imcome <= 30

```

Aggiungere una 4ta colonna in questo modo è molto utile se si vuole modificare i dati in modo programmatico e mantenere il dato originale.



Nel SQL si può avitare di dover scrivere tutte le righe se la selection vuole mantenere tutto:

```sql
select *
from person
where person.age < 30
```

Se non si mette la `where` si esegue solo una proiezione, ovvero quando restituisco tutto.

La relazione più facile è quella che restituisce tutto dal database.



## Operazioni sugli attributi che sputiamo fuori

```sql
select income/2 as semesterIncome
from person
where name = 'Luigi'
```

Questa operazione divide `income`, dalla tabella `person`, quando `name == 'Luigi'`:

| person.name | person.age | ...  | income    | semesterIncome          |
| ----------- | ---------- | ---- | --------- | ----------------------- |
|             |            |      | stipendio | stipendio/2 (annuale/2) |



###### ### Complex conditio in the “where” clause

...



## “`Like`” conditions

```sql
select *
from person
where name like 'A_d%' -- descrive un pattern
```

`A_d%`: A maiuscola prima, seconda lettera a caso, la 3za `d` minuscola e poi qualsiasi cosa.

`like ‘%~%’` per identificare valori che sono stringhe. 



## `NULL` values

Visto che la logica relazionale permette la creazione di questi valori.

```sql
select *
from employees
where age > 40 or age is null -- se non faccio questa operazione, se becco una riga `null` allora, il risultato in questa relazione è un valore non noto. Operazione risulta sconosciuta
```

=> se la risposta non è sicura la risposta viene buttata, risponde solo alle affermazioni di cui è certo.

Da notare l’utilizzo di `is`, che è diverso da `age = null`. Questa operazione può portare a effetti non conosciuti se si utilizza il `=`, siccome `age == null` se ` age` è `null`, allora `null == null`, operazione che non può essere svolta.

L’`=` confronta due oggetti appartenenti allo stesso dominio, e quindi non si può fare questa operazione con il `null`.



## Come si possono fare le Join

Questa operazione si fa specificando più tabelle nella clausola from:

Abbiamo due relazioni $R_1(A_1, A_2)$ e $R_2(A_3, A_4)$.

SQL per questa operazione è l’implementazione del prodotto cartesiano, quindi crea tutte le combinazioni:

```sql
select R1.A1, R2.A4
from R1, R2 -- esegui il prodotto cartesiano
where R1.A2 = R2.A3
```

tenicamente questa è l’operazione $\pi_{A_1, A_2}(R_1\Join_{A_2 : A_3} R_2)$.

Di solito però questa operazione non viene **mai** fatta dal DBMS, ma l’operazione finale risulta uguale a questa interrogazione.



## Exercise 5

```sql
select c.name, c.income, p.income
from person p, isFather t, person c
where p.name = t.father and
		t.child = c.name and
		c.income > p.income
```



## Explicit Join

```sql
select mother, isFather.child
, father
from isMother join isFather on 
	isFather.child = isMother.child -- prendi isMother è fai la join se isFather.child == isMother.child.
```

poi nella `where` si possono aggiungere tutte le informazioni che non hanno direttamente a che fare con la join.



## isFather $\Join$ isMother

SQL:

```sql
select isFather.child, father, mother
	from isMother join isFather on
		isFather.child = 
```

