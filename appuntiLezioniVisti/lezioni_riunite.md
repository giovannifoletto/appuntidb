Lezione Database 22 Novembre
============================

Il database quando deve fare un operazione la scompone e calcola il
costo dell'operazione totale.

Costo per le operazioni
-----------------------

\$ R(A,`\bar `{=tex}B) `\Join `{=tex}S(`\bar `{=tex}B, C)\$ =\> che ha
costo di $P_R + |R| * (L_{B+1})$.

La proiezione con il costo $1+L_B + 1$. Ma questo costo io non la ho,
siccome questa relazione viene costruita un pezzo alla volta. I valori
in uscita dall'operazione di $\Join$ vengono mandati al'operazione di
proiezione.

Ogni volta che viene eseguita l'operazione quindi si applicano entrambe,
senza avere necessità di rileggere le pagine. Questa operazione ha costo
zero.

L'operazione dopo è una $\Join$, ma l'unico modo per avere questa
operazione è un prodotto cartesiano, siccome si utilizzano dei dati che
non hanno nulla in comune.

Tutto il resto quindi sono operazioni che possono essere fatte al volo
con una singola lettura. Di fatto quindi tutta questa operazione viene
eseguita in un tempo di \$

Di solito il problema dei DBMS è che le combinazione di applicazione di
questi operatori sono potenzialmente infinite. Di fatto loro non provano
tutte le possibili, ma solo un sottoinsieme che secondo loro sono i
migliori.

``` {.sqlite}
SELECT *
FROM Train
WHERE prodotti LIKE '%7085%'
```

Ricerca di un prodotto nel quale compare un prodotto con un valore al
suo interno di $\%7085\%$.

I tempi sono uguali sia che sia un B+Tree, siccome l'unico modo per
questa ricerca è uno scan completo.

In questo modo non si può fare altro che passare tutti i valori
disponibili. Questo avviene perché non ho un prefisso chiaro sul
prodotto, ma se solo avessi un valore iniziale allora basterebbe una
ricerca attraverso un albero.

Database 27 Settembre 2021
==========================

Collegare la parte di progettazione nello schema relazionale. Questa
sezione si chiama **livello logico**, un middleware tra le viste e lo
schema.

Entity sets to tables:
----------------------

``` {.mysql}
CREATE TABLE Employees
    ( ssn CHAR(11),
   name VARCHAR(20),
   lot INTEGER,
   PRIMARY KEY(ssn) // Questa colonna è chiave primaria, quindi non esistono due elementi con stessa ssn
   // Se avessi voluto più di un elemento unico nella tabella, avrei scritto 
   PRIMARY KEY(ssn, did)
  );
```

**Come si creano le relazioni?**

Per memorizzare le coppie di questa relazione, devo fare delle tabelle
con i dipartimenti:

``` {.mysql}
CREATE TABLE Dept ...;

// Memorizzo le coppie di questa relazione in una tabella a parte:
CREATE TABLE Works_in(
    ssn CHAR(10), // Questi sono gli attributi della tabella:
  did INTEGER,  // Ospita 3 colonne: ssn, did, since 
  since DATE,   // fino a questo punto, questa è una tabella libera, posso metterci dentro tutto quello che voglio, senza nessuna limitazione => la data in cui il dipenente ha iniziato a lavorare devono essere uniche
  // Dichiarazione dei vincoli
  PRIMARY KEY(ssn, did) // impedisce la creazione di valori con ssn e did già appartenenti alla tabella, questa coppia può esistere al massimo 1 volta!
  // di solito questa prerogativa è obbligata nel modello matematico, ma di solito il DBMS non la applica di default per questioni di efficienza
  FOREIGN KEY(ssn) // Questi valori devono per forza essere presenti nella colonna 
    REFERENCES Employees, // che fa rifermento alla colonna degli impiegati
  FOREIGN KEY(did)
    REFERENCES DEpartments
);
```

``` {.mysql}
CREATE TABLE Departments(
    did INTEGER,
    dname CHAR(20),
    budget REAL,
    ssn CHAR(11),
    since DATE,
    PRIMARY KEY(did),
    FOREIGN KEY(ssn)
        REFERENCESS Employees
);
// Questa noccepiace
CREATE TABLE Manages(
    ssn CHAR(11),
  did INTEGER,
  since DATE,
  PRIMARY KEY(did),
  FOREIGN KEY (ssn)
    REFERENCES Employees,
  FOREIGN KEY(did)
    REFERENCES Departments,
);
```

``` {.mysql}
CREATE TABLE Departments(
    did INTEGER,
  dname VARCHAR(20),
  budget REAL,
  ssn CHAR(11) NOT NULL,
  since DATE,
  PRIMARY KEY(did), UNIQUE (ssn) // in questo modo, non posso avere due ssn uguali
  FOREIGN KEY(ssn) REFERENCES Employees,
    ON DELETE NO ACTION // Questo equivale a lasciare vuoto il "ON DELETE"
);
```

### *weak entity sets*: esistono solo in funzione della chiave primaria della tabella a cui sono connessi

è rappresentato da un quadrato nero scuro e ha necessità di avere una
freccia marcata in uscita (siccome ha necessariamente bisogno di una
chiave primaria).

``` {.mysql}
CREATE TABLE DEpendants(
    dname VARCHAR(20),
  age INTEGER,
  cost REAL,
  ssn CHAR(11) NOT NULL,
  PRIMARY KEY (pname, ssn),
  FOREIGN KEY(ssn) REFERENCES EMployees,
    ON DELETE CASCADE, // Quando elimino una entità nella tabella esterna, devo eliminare tutte le righe a cui sono collegati i valori
);
```

Lezione 11 Ottobre
==================

``` {.sql}
-- Voli, aerei e piloti - Esercitazione Algebra relazionale

Voli(codV: int, origin: string, dest: string, distanza: int, ora_part: time, ora_arrivo: time)

Aerei(codA: int, name: string, dist_perc:int)

Certificati(codD: int, codA: int) -- codD vincolo chiave esterna con Dipendenti 
                                  -- codA vincolo chiave esterna Aerei

-- Non tutti possono volare con un certo aereo, ma devono essere certificati per un determinato tipo di aereo
Dipendenti(codD: int, name:string, stipendio:int)
```

1)  Piloti certificati per qualche aereo:

$\pi_{codD}(Certificati)$

``` {.sql}
-- Relazione certificati e proiettare sulla colonna dei codici
Sid | CodA
1   | a1
2   | a2

CodD
1
2
```

$\pi_{codD}(Certificati) \Join Dipendenti$, ma non voglio associare
tutti a tutti, ma voglio agganciare alla riga con valore 1, soltanto la
riga con codice 1. (Questo posso fare con **join naturale** =\>
`codeD destra == codeD destra`)

$\rho(DipCert, \pi_{CodD} (Certificati))$, ovvero do un nome a questa
relazione/operazione.

$DipCert \Join _{DipCert-codeD =Dipendenti-codeD} Dipendenti$, ovvero
restituisce della join, solo quei valori per cui
`Dipcert_codeD == Dipendenti_codeD`.

Questa operazione ritorna:

  CodeD (1)   codeD(2)   NameD   Stipendi
  ----------- ---------- ------- -----------
  1           1          nome    stipendio
  2           2          nome    stipendio
  ...         ...        ...     ...

Questo permette di avere un'operazione join solo con i valori che sono
in comune.

Poi posso creare

  CodeD   ..
  ------- -----
  ---     ---

Questa operazione posso farla anche in un colpo solo, con
$\pi_{codD}(Certificati \Join Dipendenti)$.

2)  Voglio gli aerei che possono volare non stop da 'Bonn' a 'Madrid':

=\> Voglio i codici degli aerei la cui autonomia è maggiore o uguale a
Bonn-Madrid

3)  Voli effettuabili da qualche pilota con sitpendio \> 10000\$

$\rho(VoloAereo, \pi_{codV, CodA}(Vdi \Join_{distranza \le dist} Aerei))$

  CodV   CodA
  ------ ------
  1      2

poi faccio
$\pi_{cod 1)}(\delta _{stipendio \le 100000}(Dipendenti)) \Join Certificati$:

  codeD   Cod 1)
  ------- --------
          

Tutto sto casino per trovare gli aerei pilotabili con uno stipendio
alto.

$\pi_{codV}(Voli \Join \delta_{stipendio \le 100000} (( Aerei \Join Certificati \Join Dipendenti)))$

4)  Nomi dei piloti che possono pilotare aerei con distanzaperc\>=3000KM

modo 1) join naturale "tutti e tre"
$((Dipendenti \Join certificati) \Join Aerei)$

questa selezione crea una ENORME tabella con i seguenti attributi:

\|codD, named, "qualcosa che non riesco a leggere", codA, nome,
dist.perc\|

ora dobbiamo solo selezionare quello che ci serve, ovvero: le righe che
parlano di aerei con distanza percorribile \>= 3000km

$\pi codD(\delta(Dipendenti \Join certificati) \Join Aerei)$

$\rho (Dip3000 \pi nomeA(\pi codD(certificati\Join(\pi codA( \delta dist.perc >= Aerei)))\Join Dipendenti)$

4.1) Nomi dei piloti che possono pilotare aerei con
distanzaperc\>=3000KM ma che non hanno nessun certificato per Boeing
(adesso sta dicendo un sacco di cose che sono troppo addormentato per
scrivere)

$\rho(DipNO \pi codD(\delta name: Boeing(certificati \Join Aerei))$
$Dip3000 - DipNO$

il salario massimo in SQL si richiedere così:

``` {.sql}
(SELECT codD FROM dipendenti HAVING salario = MAX(salario))
```

La cosa che l'algebra relazionale non può fare è contare le tuple della
mia relazione. In SQL si può fare, ma solo perchè viene implementata con
una sezione di codice, ma non ci sono relazioni matematiche.

Lezione 13 Oct
==============

``` {.sql}
Attori(codA: int, nome:string, annoNasc: int, naz: string)
Film(cadF: int, titolo: string, durata: int, regista: int)
Registi(codR: int, nome: string, annoN: int)
Recita(attore: int, film: int, ruolo: string)

Recita[attore] FK to Attori
Recita[film] FK to Film
Film[registi] FK to Registi
```

1.  Nome degli attori che hanno recitato in almeno un film

    $\pi_{nome}(Recita \Join_{attore:\space codA} Attori)$

2.  Nomi di attori che hanno recitato in almeno due film

    $\rho(R1, Recita)$, $\rho(R1, Recita)$,
    $R1 \Join_{R1.attore \space: \space R2.attore } R2$

    $\pi_{R1.attore}(R1 \Join_{R1.attore \space : \space R2.attore < R1.film \pm R2.film} R2) \Join_{attore: codA} Attori$

3.  Nome degli attori che hanno recitato in esattamente un film

    $\rho(Almeno1, \pi_{attore}(Recita))$

    $\rho(Almeno2, \pi_{Recita}(R1 \Join_{R1.attore: \space R2.attore < R1.film \pm R2.film} R2) )$

    $\pi_{nome}( (Almeno1-Almeno2) \Join_{attore = codA} Attori)$

4.  Attori che non hanno interpretato qualche ruolo

    $\rho(R1, Recita)$, $\rho(R2, Recita)$.
    $\pi _{R1.attore}(R1 \Join_{R1.attore = R2.attore < R1.ruolo \pm R2.ruolo} R2)$

    $\pi_{codA}(\pi_{codA}(Attori) \times \pi_{ruolo}(Recita) - \pi_{attore, \space ruolo}(Recita))$

5.  Attori che hanno recitato in tutti i ruoli

    $\rho(AttoriMancaR, )$

    $\pi_codA(Attori) - AttoriMancaR)$

    $\pi_{attore, ruolo} (Recita) \div \pi_{ruolo}(Recita)$

6.  Registi che non hanno mai diretto film in cui non ha mai partecipato
    DeNiro

    $\sigma_{nome = DeNiro}(Recita \Join_{attore: codA} Attori)$

      attore   film   ruolo   CodA   Nome     annoN   sez
      -------- ------ ------- ------ -------- ------- -----
      a1       f1     ..1     a1     DeNiro   ..      ..

    Film in cui che ha recitato deNiro, più un sacco di informazioni che
    non ci interessano.

    $\sigma_{nome = DeNiro}(Recita \Join_{attore: codA} Attori) \Join_{film=codF} Film$

    $\pi_{CodR}(Registi) - RegNo$

    $\pi_{regista}(Film) - RegNo$

7.  ...

    \$`\rho`{=tex}(B, `\pi`{=tex}()) \# 18 Oct. 2021

SQL statement
-------------

``` {.sql}
select 
    from -- Si chiamano clausole, indicano da dove prendere i dati
    where -- con che modalità prendere i dati
```

Risultato è una relazione con gli attributi messi in input.

$\pi_{name, income} (\sigma _{age< 30} (person))$ Questa query in
algebra, si traduce in:

``` {.sql}
select person.name, person.income
from person
where person.age < 30
```

Questa operazione è che prende una tabella e inizia a scansionare le
righe, poi la where indica le clausole per tenere o buttare una riga.

Notiamo che l'attributo viene inserito con il nome della tabella prima :
`tabelName.objectName`. Se non ci sono ambiguità allora si può evitare
tutto.

Posso anche rinominare degli oggetti per l'interrogazione:

``` {.sql}
select p.name as name, p.income as income
from person as p -- rename persone as p
where p.age < 30
```

Inoltre il `p.name as name` è un modo per cambiare il nome delle
colonne.

Il risultato di operazione per SQL non rimuove i duplicati, perché è
troppo costosa dal punto di vista computazionale. Per avere un elenco di
elementi unici, allora devo:

``` {.sql}
select surname, city from employees -- not remove duplicates

select distinct surname, city from employees -- removes duplicates
```

Tutti altri operatori sono poi definiti come un normale linguaggio di
programmazione:

``` {.sql}
select name, age, income
    income as repeatedIncome -- sto aggiungendo una 4ta tabella con la copia dello stipendio
from person
where income >= 20 and imcome <= 30
```

Aggiungere una 4ta colonna in questo modo è molto utile se si vuole
modificare i dati in modo programmatico e mantenere il dato originale.

Nel SQL si può avitare di dover scrivere tutte le righe se la selection
vuole mantenere tutto:

``` {.sql}
select *
from person
where person.age < 30
```

Se non si mette la `where` si esegue solo una proiezione, ovvero quando
restituisco tutto.

La relazione più facile è quella che restituisce tutto dal database.

Operazioni sugli attributi che sputiamo fuori
---------------------------------------------

``` {.sql}
select income/2 as semesterIncome
from person
where name = 'Luigi'
```

Questa operazione divide `income`, dalla tabella `person`, quando
`name == 'Luigi'`:

  person.name   person.age   ...   income      semesterIncome
  ------------- ------------ ----- ----------- -------------------------
                                   stipendio   stipendio/2 (annuale/2)

###### \#\#\# Complex conditio in the "where" clause

...

"`Like`" conditions
-------------------

``` {.sql}
select *
from person
where name like 'A_d%' -- descrive un pattern
```

`A_d%`: A maiuscola prima, seconda lettera a caso, la 3za `d` minuscola
e poi qualsiasi cosa.

`like ‘%~%’` per identificare valori che sono stringhe.

`NULL` values
-------------

Visto che la logica relazionale permette la creazione di questi valori.

``` {.sql}
select *
from employees
where age > 40 or age is null -- se non faccio questa operazione, se becco una riga `null` allora, il risultato in questa relazione è un valore non noto. Operazione risulta sconosciuta
```

=\> se la risposta non è sicura la risposta viene buttata, risponde solo
alle affermazioni di cui è certo.

Da notare l'utilizzo di `is`, che è diverso da `age = null`. Questa
operazione può portare a effetti non conosciuti se si utilizza il `=`,
siccome `age == null` se `age` è `null`, allora `null == null`,
operazione che non può essere svolta.

L'`=` confronta due oggetti appartenenti allo stesso dominio, e quindi
non si può fare questa operazione con il `null`.

Come si possono fare le Join
----------------------------

Questa operazione si fa specificando più tabelle nella clausola from:

Abbiamo due relazioni $R_1(A_1, A_2)$ e $R_2(A_3, A_4)$.

SQL per questa operazione è l'implementazione del prodotto cartesiano,
quindi crea tutte le combinazioni:

``` {.sql}
select R1.A1, R2.A4
from R1, R2 -- esegui il prodotto cartesiano
where R1.A2 = R2.A3
```

tenicamente questa è l'operazione
$\pi_{A_1, A_2}(R_1\Join_{A_2 : A_3} R_2)$.

Di solito però questa operazione non viene **mai** fatta dal DBMS, ma
l'operazione finale risulta uguale a questa interrogazione.

Exercise 5
----------

``` {.sql}
select c.name, c.income, p.income
from person p, isFather t, person c
where p.name = t.father and
        t.child = c.name and
        c.income > p.income
```

Explicit Join
-------------

``` {.sql}
select mother, isFather.child
, father
from isMother join isFather on 
    isFather.child = isMother.child -- prendi isMother è fai la join se isFather.child == isMother.child.
```

poi nella `where` si possono aggiungere tutte le informazioni che non
hanno direttamente a che fare con la join.

isFather $\Join$ isMother
-------------------------

SQL:

``` {.sql}
select isFather.child, father, mother
    from isMother join isFather on
        isFather.child = 
```

Database 25 Oct. 2021
=====================

Teoria di SQL

$$ isFather \Join isMother $$

Left Outer Join (oppure solo Left)
----------------------------------

``` {.sql}
select isFather.child, father, mother
from isFather left outer join isMother -- oppure
    isFather left join isMother
    on isFather.child = isMother.child
```

Questa richiesta fa in modo che gli attributi della tabella, da priorità
alla sinistra, e quelle che può essere `null` è la tabella di destra. Se
trova un valore con tutti allora restituisce, se trova un valore di
destra (`isMother` ) e di sinistra (`isFather`). Prende una riga dalla
destra, se c'è il match sulla sinistra allora si restituisce la riga
della join, se non si trova nulla, allora non restituire, ma se trovi
`null` solo sulla tabella di sinistra allora restituisci comunque.

Allo stesso modo la `right outer join`, che funziona uguale ma nella
direzione opposta.

Full outer join
---------------

Questa query non da prorità all destra o la sinistra, ma basta un
attributo (che sia di destra o di sinistra) `null`. In questo modo si
applica lo stesso, e quindi restituisce l'unione tra `left` e
`right outer join`.

Order By
--------

dichiara l'attributo `name` nella tabella risultante, e poi in ordine
crescente (nel caso delle stringhe in ordine alfabetico).

``` {.sql}
select name, income
from person
where age < 30
order by name
```

se avessi due `name` uguali, allora poi riordina per `income` dopo.

Se volessi ordinare con `name` e `income` uguali, allora è necessario
dichiarare `distinct`:

``` {.sql}
select distinct name, income
from person
where age < 30
order by name
```

Si può anche aggiungere `limit <n>`, per limitare ai risultati:

``` {.sql}
select distinct name, income
from person
where age < 30
limit 2
```

di solito questo si utilizza in concomitanza con l'`order by`, ad
esempio per trovare i *top 10 workers*.

Aggregate operators
-------------------

`count, min, max, average, total`, utilizzati con questa sintassi:

``` {.sql}
Function( [distinct] Attributes)
```

Esempio:

``` {.sql}
select count(*) as NumChildFranco
from isFather
where father = 'Franco'
```

Questa query risulta il numero delle colonne che includono Franco.

Quando non si fa il `count(*)` ma solo sul singolo attributo, allora si
ignora il valore `null`.

Operatore `sum`: questa funzione ha senso solo se si ha un attributo.

``` {.sql}
select avg(income)
from person join isFather on
    name = child
where father = 'Franco'
```

Questo operatore ignora i valori `null`. Considera il `distrinct` e
quindi più occorrenze vengono considerate se non dichiarato il
contrario.

Operatore `group by`:

``` {.sql}
select father, count(*) as NumChildren
from isFather
group by father
```

Conditions on Group
-------------------

Clausola `having` si usa sempre con la `group by`. Ha la stessa funzione
della `select`.

``` {.sql}
select father, avg(c.income)
from person c join isFather
    on child = name
group by father
having avg(c.income) > 25
```

Questa keyword è un modo per fare un aggregato e poi fare una funzione.

La `having` si può usare anche senza `group by`, ma in questo modo sto
dicendo che il gruppo è tutta la tabella, e l'unica cosa che posso
scrivere nella `having` è l'aggregato.

Riassunto Sintassi `Select`
===========================

``` {.sql}
select AttributeOrExpressionList
from TableList
where SimpleConditions
group by GroupAttributeList
having AggregationsConditions
order by OrderingAttributeList
limit Number
```

Implementazione degli operatori insiemistici
============================================

Operatore Union
---------------

``` {.sql}
select * 
from R
union 
select *
from S 
```

Nel formalismo matematico, si ottiene con questo: si ottengono, da **due
tabelle union-compatible** allora si ottiene questa funzione.

**union all** serve invece a fare una union senza controllo dei
duplicati, ovvero fa anche i duplicati. Se non ci interessa
dell'efficienza e della velocità di esecuzione allora possiamo usare
sempre union all.

Nested Queries
--------------

Voglio i genitori di persone che si chiamano franco:

``` {.sql}
select name, income
from person, isFather
where name = father and child = 'Franco'

select name, income
from person
where name = (select father   -- controlla che il father di chi ha come figlio 'Franco'
                from isFather -- = per il risultato -> se no devo usare `in`
                where child = 'Franco')
```

Queste sotto-query sono possibili anche attraverso `any` o `all`. Questa
keyword ha lo stesso significato della query sopra, ma sa come trattare
più risultati dalla sottocondizione.

Questa keyword si può applicare anche con degli altri operatori:

``` {.sql}
select name, income
from person
where name <operator> any(select father
                            from isFather
                            where child = 'Franco')
           <> -- diverso
```

Es: name and income of the fathers of persons earning more than 20
milions

``` {.sql}
select disting p.name, p.income
from person p, isFather, person c
where p.name = father and child = c.name
    and c.income > 20
    
-- questa sotto è uguale, ma come se inizio a metter un po' di ordine
select name, income
from person
where name in (select father -- padre di persone che hanno stipendio meno di 20 mil.
                from isFather
                where child in (select name -- persone che guadagnano > 20 mil. 
                                from person
                                where income > 20))
```

Questo metodo ha il lato positivo di semplificare la scrittura e la
lettura di questa `query`, ma il DBMS non sa come ottimizzare questa
query, siccome gli si sta dando un ordine di esecuzione definito
dall'users. Questo metodo blocca l'esecuzione unica di una query.

Lato positivo: maggiore leggibilità del codice

Lato negativo: meno efficiente per il DBMS.

Miglioramento prestazione
=========================

RAM: molto meno spazio, ma super veloce (3gb/s anche). Non si può
pensare di avere un intero DB all'interno della ram.

HARDDISK: molto più spazio, ma alto tempo di lettura (500Mb/sec
all'incirca). Questo è perché una testina meccanica deve leggere i file
in modo fisico. Di solito per questo motivo ci sono delle tecniche per
recuperare tutti i dati vicini a questo, prendendo tutto il **settore**
dell'harddisk. Di fatto quindi questo disco di memoria è una sequenza di
settori. Questo spazio è così piccolo che non riesce a memorizzare la
tupla.

Per questo motivo organizza i settori in blocchi o **pagina** =\> 8kbyte
del disco (16 settori fisici del disco).

Quando postgress accede al disco allora leggerà tutto il contenuto della
pagina, in modo da avere i vicini in ram. Postgress ha questa
informazione hard-coded nella pagina, e quindi bisogna ricompilare il
programma per fare queste cose. Ogni pagina che leggo vuol dire che
leggo tutta la pagina. Modello di costo è basato sulle pagine che
leggiamo e scriviamo.

Il DBMS sa quando il file inizia e finisce, perché sa quanto è grande il
file. Con queste informazioni, e con le informazioni della dimensione
della pagina, allora si può sapere dove arrivare. Se le pagine sono
singole e collegate tra loro con un puntatore, allora non potrei mai
sapere dove sono localizzati i file.

Ma tutte queste informazioni vengono spostate nella ram? Si.

Database di questo tipo:

``` {.sql}
R (id INTEGER, num INTEGER, city VARCHAR, age VARCHAR); 
```

Logicamente postgress è un programma e non può utilizzare tutto il disco
da solo, quindi prenderò una sola sezione.

Spazio di calcolo =\> dimensione della tupla $t_R$, che indica le
dimensioni della tupla del DBMS, calcolando che questo sia costante per
tutto il DB.

Questa tupla viene inserita nel DB attraverso la creazione di una
pagina. Di solito non è conveniente che si abbia una tupla a metà tra
due pagine. Questo viene fatto perché il tempo di recuperare due pagine
non conviene rispetto alla perdita di un po' di spazio. Questo modo di
organizzare le informazioni viene chiamato HEAP-FILE: file non
organizzati in nessun modo, senza nessun tipo di relazioni di ordine, ma
semplicemente inserisci come viene letto.

Se in questo momento si deve fare una query, allora devo leggere tutte
le pagine. Il tempo di calcolo di queste operazioni del processore è
infinitesimo rispetto al tempo necessario e recuperare i file necessari.
Il costo di questa operazione è $P_R$. Quindi si dice che il costo di
**scan** è $P_R$ (il numero di pagine della relazione).

L'operazione di ricerca in un database si chiama **equality search**, e
di base rispecchia questo codice:

``` {.sql}
Select *
from R
where city='Trento';
```

Quando si fa una *query* con questo stato dei dati, il costo è sempre il
costo di lettura di tutte le informazioni.

Inserimento dei dati è **insert**, rispecchia la chiamata in sql:

``` {.sql}
Insert into R value (--- Tupla ---);
```

Questo costo, con questo stato di dati dovrò fare una serie di
operazioni:

-   carico l'ultima pagina in ram
-   controllo che questa pagina sia vuota o piena
    -   se è piena, scarto dalla ram la pagina, creo una nuova pagina
        con sopra la tupla nel file
    -   se è vuota allora inserisco e basta

di fatto questo vuol dire che il costo unitario di questa operazione è
2. Il fatto che questa operazione abbia un costo così basso fa in modo
che un database con un inserimento costante abbia migliore tempo di
apertura.

Ultima operazione è **delete**, che corrisponde alla funzione sql:

``` {.sql}
DELETE
FROM R
WHERE city=`Trento`;
```

Leggere tutte le pagine del file, ma scriverò soltanto quelle pagine
dove ho trovato `Trento`. Il costo di questa operazione allora è \$P\_R
+ \|R\_{city=`Trento`}\| \$ (Questo indica il numero di attributi il cui
attributo city è uguale a trento). Nella peggiore delle ipotesi ogni
persona sta in una pagina diversa (Caso peggiore). Alla peggio dobbiamo
riscrivere questo quantitativo di valori.

Le pagine vuote sono memorizzate nel DBMS e, con un sacco di
implementazioni, si possono poi ririempire. Ignoriamo i costi di
rimettere a posto il file dopo questa operazione.

Il DBMS inoltre conosce il valore di $P_R$, ma spesso i DBMS utilizzano
delle statistiche non aggiornate in tempo reale per dare un valore al
resto dell'operazione. Molto spesso queste assunzioni non vengono fatte.
Un assunzione tipo invece che potrebbe essere utile è ad esempio si
utilizza questa formula: $$
\frac{|R|}{|R_{city}|}
$$ Ma se voglio analizzare questo fattore, allora: $$
\frac{1}{|R_{city}|} = \text{selectivity factor}
$$ Questo valore indica quanto ogni tupla

Mi sono perso recupera questa parte

Costo di questa operazione è \$P\_R + \|R\_{city = Trento}\| = P\_R +
`\ulcorner `{=tex}f \* \| \$

**Lati positivi**: inserimento costo unitario e sempre minimo

**Lati negativi**: tutte le altre operazioni hanno bisogno di uno scan
completo

Miglioramento
-------------

Avere le tuple organizzate in una maniera che utilizza l'ordine della
chiave che dovrei utilizzare di più. Questo permette l'applicazione del
binary search.

Il costo di trovare questi valori diventa
$\ulcorner log_2(P_R)+ \frac{|R_{city = Trento}|}{\inf(\frac{P}{t_R})}$.
Di fatto si fa uno *scan* dal valore ritornato dalla ricerca binaria
(che ritorna il primo elemento del valore cercato, e poi il primo valore
spurio).

Prima dobbiamo sapere quanto è grande una tupla e quanto è grande una
pagina. Facendo questa operazione, se io faccio $\inf (\frac{P}{t_R})$.
Questo calcolo permette di fare il calcolo per sapere quante ne devo
leggere in ogni pagina.

Quando decido di utilizzare un sorted file il criterio va deciso con un
senso.

**scan in un sorted-file**: non migliora lo status. Bisogna sempre
scorrerlo tutto.

**Range search**: sposto il puntatore di ricerca in avanti uno spazio
prefissato finché non trova un valore simile, poi prende tutto.

#### Insert

con la ricerca binaria trovo il valore della pagina dove dovrebbe
trovarsi. Questa pagina è in memoria in ram, quindi non ha più costo
adesso. $log(P_R) + 1$. La nuova pagina sarebbe così, siccome la prima
pagina ha ancora spazio.

Se non c'è spazio? come si fa a aggiungere una pagina in mezzo.
**Inserimento molto dispendioso** siccome la tabella ha un costo di
rimettere la tabella a posto è ignorato da noi, ma in un caso reale è
molto consistente.

#### Delete

Voglio eliminare tutte le persone con età a 50 anni. La ricerca ha costo
logaritmico come al solito. Poi ho: $$
\inf(log_2(P_R)) + 2 \inf(\frac{|R_{city = Trento}|}{\inf(\frac{P}{t_R})})
$$

### Altro miglioramento

Nel sorted file abbiamo un costo ingombrante che è quando rimetto a
posto il file in modo da essere consistente.

Si utilizzano gli indici. Questo dati vengono memorizzati su un altro
file diverso. Questo nome prende spunto dall'indice analitico dei libri.

Lo scopo degli indici è che le tuple non siano per forza in ordine, ma
che indichi dove ci sono le pagine che hanno questo valore in comune.

Ci sono due tipi:

-   hash:
    -   si utilizzano gli hashtable
-   alberi di ricerca
    -   B+ tree.

Postgress utilizza i B+ tree. Il modo più facile e utile da vedere è
come postgress lo utilizza e come funziona.

Si utilizza una chiave che deve essere molto efficiente in una serie di
operazioni, e si crea un B+ tree con quel tipo di chiave come base.

Si creano due file: data file, index file. Il primo contiene i dati, il
secondo è l'indice analitico dei file.

Quando si inserisce la tupla, lo si fa nel primo posto libero, ma
nell'index file aggiungo un altra indicazione con il valore della tupla
e l'indirizzo della pagina dove si trova questa tupla (offset).

Quando aggiungo un altro valore, allora aggiungo il valore al file di
indice. Se io adesso voglio cercare le tuple con valore 20, allora seguo
il puntatore e leggo la pagina.

Le tuple nel datafile non sono ordinate, ma nell'indice si. Nell'indice
però non è presente tutta la chiave di ricerca, ma solo la chiave con
l'indirizzo di riferimento. In questo modo faccio una ricerca ordinata
sul file di index.

Si crea un'albero binario+ con le foglie alla base. Tutto è collegato in
modo da essere sempre consecutivo.

Lezione 15 Nov
==============

Index file che ha una fila di puntatori alle pagine del datafile.

Ad eccezione della radice, ogni nodo ha un numero almeno metà del
massimo.

Unclustered B-tree
------------------

variante del unclu. è il

Clustered Data-Tree
-------------------

In questo albero, le foglie che partono da questo dato vanno tutte
insieme. Quello che abbiamo è che i puntatori alla tupla sono
raggruppati.

Questi valori sono nella stessa pagina, se finisce lo spazio, altre
tuple vicine sono raggruppate in un'altro spazio. Per questo motivo
viene definito clustered.

Bisogna leggere il numero di pagine che contengono tutti le tuple
cercate.

Modifica e eliminazione: praticamente uguale alla ricerca la ocmplessità
ma con un costo aggiuntivo per la modifica

Funzione di Hash
----------------

Funzione che restituisce un valore univoco. \# Lezione 17 Nov.

Esecuzione delle join

#### Nested Loop Join

Questo valore viene calcolato con due loop nestati (doppio for).

Primo cicla sulle tuple e poi il secondo cicla per il controllo.

``` {.java}
foreach tupla t1 in R:
    foreach tupla t2 in S:
        if t1.A == t2.B:
            print t1, t2
```

Il costo di questa operazione è:

-   per ogni riga =\> leggo tutte le tuple dell'altra tabella

\$\$ 1 + \|S\| + \\ 1 + \|S\| + \\ ... \\ ... \\ 1 + \|S\| \\

`\to `{=tex}\|R\| + \|R\|\*\|S\| \$\$

Questo risultato viene ottenuto perché si stanno leggendo le letture e
le scritture.

Ma noi sappiamo anche che ogni lettura equivale a una pagina, non ogni
riga. Questo calcolo posso semplificare in questo modo:

-   siccome devo leggere ogni pagina, allora posso fare in questo modo:

-   prendo la prima pagina di R

-   prendo la prima pagina di S

-   adesso posso leggere e scrivere con costo zero, e di fatto posso
    anche trovare tutte le tuple di *join*

-   poi posso seguire con la pagina seguente

-   facendo in questo modo si ottengono i risultati =\> una volta che è
    a schermo si pensa come spazio libero in ram, siccome l'utente ha
    letto ormai.

-   Questo calcolo quindi ha tempo di esecuzione: $$
    P_r \times ( 1 + P_s \\
    1 + P_s \\
    .... \\ 
    1 + P_s ) \\ 
    $$ Questo è il costo per fare la join, ovvero $P_r + P_r*P_s$.

-   Siccome il costo dipende dalla lunghezza di una delle due, allora il
    DBMS prende la tabella con meno colonne in assoluto.

-   Questo algoritmo funziona sempre, non cambia in base alla struttura
    del DBMS, in modo da essere sempre capace di fare le join. é molto
    probabile che si utilizzi questo modello se non si hanno degli
    indici particolari, o una variante ottimizzata.

-   Per questo motivo l'operazione di Join è costosa: dipende dal
    prodotto delle due.

#### Sort-merge Join

Per migliorare l'algoritmo di prima, che rimane valido sempre in
generale. Ma adesso possiamo ridurre un filo il tempo che impiega questa
azione usando il fatto che:

Se la tabella è ordinata non c'è nessun costo collegato al controllo che
questa tabella sia ordinata o meno.

Se le tabelle non sono ordinate, allora prima paga il costo di mettere
in ordine le tabelle in base ai due criteri cercati. Questo costo può
non essere indifferente.

Ora che sappiamo che i valori sono uguali, allora è come se guardassi le
prime due tuple della tabella. Se una è più piccola dell'altra, allora
non c'è modo di eseguire l'operazione di Join. Quando uno dei due valori
è più grande dell'altro, allora mando avanti il più piccolo. Seguo
questo controllo finchè non ottengo due tuple con lo stesso valore.
(assumiamo che non si applichino duplicati).

Una volta che si trovano due valori uguali, ottengo la join tra i
valori, poi continuo come se fossi appena partito eseguendo sempre il
controllo di prima.

Se la tabella di partenza è finita, allora ho finito tutta la relazione.

Il costo di questa operazione è che ho letto tutte le tuple di $R$ una
volta sola, e tutte le tuple di $S$ una volta sola. Se non ci sono dati
riordinati allora bisogna aggiungere i dati di riordino.

Se ci sono duplicati poi ??

Il costo quindi è questo: non c'è bisogno di tornare indietro su una
pagina già letta, perché una volta controllato, allora so già per certo
di averla già controllata.

Il tempo migliore dell'algoritmo per ordinamento è $N log_2(n)$. Ma
questa relazione non vale perché è calcolata con tutto l'array in ram.
Dopo puoi riscrivere le pagine su disco con un tempo lineare se
riuscissi avere tutto il db in ram. Questo valore non è possibile,
perchè non si può mai prendere il fatto che si possa avere tutto il db
in ram. Logicamente questi algoritmi allora devono lavorare con meno
dati possibili in ram e ottengono un risultato migliore possibile.

Se l'algoritmo di ordinamento ha (nel caso di postgres) a disposizione 3
pagine in ram (8kbyte \*3). L'algoritmo di ordinamento è
$2 * P_r * ( \ulcorner log_2(Pr) \urcorner + 1 )$. Questo algoritmo però
lavora con un numero di pagine predefinite in ram, invece c'è un
algoritmo che riesce a scalare il numero di pagine in ram. Se si riesce
a fare questa operazione (scalare il numero di pagine in ram) allora si
ottiene un valore più piccolo di tempo, sempre rimanendo lineare:
$2 * P_r ( \ulcorner log_{B-1}(P_R/B) \urcorner +1)$. Questo valore è
molto più piccolo siccome l'algoritmo lavora su un logaritmo molto più
basso, quindi, rimanendo lineare, si ottiene un costo temporale minore.

Per questo motivo si ha un sort-merge join. Assumendo che non si hanno
chiavi uguali all'uguaglianza si ha allora:

-   il costo per ordinare $R$ (se si vogliono 3 pagine in ram oppure se
    si vogliono più di 3 pagine in ram)
-   il costo per fare la join con le tabella ordinate

Di fatto quindi si ottiene: $$
2 * P_r ( \ulcorner log_{B-1}(P_R/B) \urcorner +1) + P_R + P_S
$$

Di solito quindi in questi casi, il DBMS cerca tra le strade le funzioni
che costano di meno e poi svolge l'azione che costa meno.

#### Hash Join

Non ha a che fare con il fatto di avere un indice di hash sulle tabelle.

Lo sforzo iniziale che dobbiamo fare per disporre i dati in modo più
efficiente. Diamo per scontato che le relazioni siano salvate sul disco.

Usiamo una funzione di hash, che riceve un valore e restituisce un
valore nel valore prefissato. Le funzioni di hash possono essere
applicate a ogni tipo di dati. Una volta che applico la funzione allora
è univoco il valore (se la funzione è fatta bene) e in questo modo è
facile rendere le tuple uniche.

A questo punto eseguo il modulo su questo valore e ottengo una tabella
con questi valori, allora ho ridistribuito su disco secondo i *buckets*.
Faccio questa operazione sia con $R$ che con $S$ con lo stesso numero di
*buckets*. In questo modo la funzione butta nello stesso *buckets* sia i
moduli con un valore di $R$ sia quelli di un valore di $S$.

In questo modo ho guadagnato che tutte le join da fare sono tutte nello
stesso buckets. Questo perché si ha il valore di hash con un tipo di
risposta uguale, allora questo valore può essere raggruppato.

Ci possono essere tuple che non hanno a fare nulla, ma so che se hanno
da fare una join, allora sarà nello stesso buckets.

Lo scopo è che questa operazione separi tutte le operazioni in due
pagine per buckets, in modo che posso caricarle entrambe in memoria e
quindi svolgere tutte le operazioni a costo infinitesimo.

Una volta che ho tutti i dati nella hashtable. Alla peggio ogni tupla
finisce in un buckets diverso. Per ognuna di queste tuple, allora devo
leggere le rige che contiene e poi riscriverle su disco.

A questo punto ho fatto una lettura sulla pagina di $R$ e poi ho finito:
\$\$ (1 + 2 \* ?\_1 \\ 1 + 2\*?\_2 \\ ... \\ ... \\ 1 + 2 \*?\_m )

`\to `{=tex}P\_R + 2 \* (?\_1 + ?\_2 + ... + ?\_m) \\ `\to `{=tex}P\_R +
2*(\|R\|) \$\$ Perché la cardinalità è il $?$: è il minimo tra
cardinalità e numero di *buckets\*.

La cosa migliore è che il buckets riesca a ottenere tutte le tuple in un
buckets solo. Se non si riesce allora si ottiene un valore un filo più
grande, e questo valore è quello necessario per ottenere tutte le tuple
di $R$,

``` {.mysql}
SELECT *
FROM R
WHERE R.name LIKE 'c%'
```

Utilizzare il B+tree, e non posso usare l'hash perché ho bisogno di
tutta la parola.

``` {.mysql}
SELECT COUNT(*)
FROM R
WHERE R.age < 30;
```

Lezione DB 29 Nov 2021
======================

Uniche dipendenze funzionali che possiamo avere sono quelle che non
permettono ridondanza.

Questo algoritmo ci permette di ottenere una decomposizione senza
perdita di informazioni e in forma normale. Senza questi due obiettivi,
l'operazione non è utile a nulla.

$R(A, B, C, D, E);$ e $S(F, G, H)$.

Con queste istanze:

$R$: $A \to CD$, $AD \to E$, $A \to E$.

  A   B   C   D   E
  --- --- --- --- ---
  1   4   1   3   6
  2   4   2   4   7
  2   4   2   5   7
  2   4   2   6   7

$S$:

  F   G   H
  --- --- ---
  1   1   1

------------------------------------------------------------------------

provo tutte le copie in $R$ e verifico $A \to CD$: Se trovi 2 *tuple* su
$A$ uguali, allora devono essere uguali in $C$ e in $D$. Queste due
tuple sono uguali anche su $C$ e su $D$? Nope, quindi il valore di $A$
non determina sempre il valore di $C$ e di $D$. Questo valore quindi non
dipende da $A$.

Ci interessa trovare le coppie di tuple dove coincide $A$ e $D$: non
esistono.

$AD \to E$: soddisfatta da questa istanza.

$A \to E$: soddisfatta da questa istanza.

L'unica cosa che ci fa capire se queste dipendenze sussistono in
generale allora bisogna analizzare lo schema, ma ci fa di certo capire
che la prima non sussiste, siccome è violata già sull'istanza di
riferimento.

$<R(A, B, C, D, E, F), AB \to CD, C\to EF>$: voglio queste dipendenze
funzionali. Lo schema relazionale è in BCNF? Per capire questo concetto
allora bisogna:

-   calcolare la chiusura (tramite ad esempio agli assiomi di ..)
-   concludere se è triviale oppure se esiste.

Definire la chiusura di questo elenco è importante, siccome può avere
tantissimi dati. Se il BCNF è chiuso rispetto all'insieme, allora sarà
tutta la tabella? ??

Come faccio a capire se questo schema è in BCNF allora devo definire
tutte le dichiarazioni triviali o formali del BCNF. Per capire questa
cosa allora bisogna ottenere guardando lo schema, altre con informazioni
esterne provenienti dallo schema.

Se l'insieme iniziale rappresenta una chiave, allora parto da un insieme
$AB$ e chiedo se la sua chiusura di tutti gli attributi: in questo caso
$AB$ sono superchiavi, ovvero non esistono tuple con $AB$ uguali. La
chiusura la calcolo in questo modo: $$
(AB)^+ = \bar{AB}CDEF \\
(A)^+ = nope \\
(B)^+ = nope
$$ =\> di fatto AB determinano a cascata anche $CDEF$. $AB$ insieme
formano una superchiave, ovvero un insieme di attributi che determinano
tutti gli altri. Se si parte solo da $A$ o da $B$ non si riesce a
ottenere una chiave candidata. Di fatto $AB$ è superchiave e non
ripeterò 100 volte la stessa relazione tra questi attributi. Questa
corrispondenza non è triviale, perché di fatto lo sarebbero solo se $AB$
fossero triviali.

$(C)^+$ non è superchiave, né tantomeno chiave candidata. Di fatto
quindi questa chiave rappresenta ridondanza. **Lo schema non è in BCNF**
(o triviali, oppure la parte sinistra è una superchiave).

Scorporo la sezione $CDE$ in un altra relazione, di fatto quindi ottengo
una divisione tra dipendenza funzionale e no:

-   $R_1(A,B,D,C)$, in cui vale la dipendenza $AB \to CD$ =\>
    $<R_1(A,B,D,C), AB \to CD>$
-   $<R_2(\bar{C}, \bar{E}, \bar F), \bar C \to \bar E\bar F>$

Per gli esempi così piccoli trovare la forma BCMF nella tabella è
piuttosto facile, non si può dire la stessa cosa con una relazione molto
grande.

$<R(A, B, C, D, E), F>$ e $F = \{A \to B, BC \to E, ED \to A\}$

Se $A$ determina $B$, allora posso scrivere in questo modo:

``` {.mermaid}
graph LR
A-->B;
B-->E;
C-->E;
E-->A;
D-->A;
```

Questo schema indica che nessun attributo singolo è chiave candidata.
Adesso partiamo ad insiemi fatti da due elementi:

-   se si parte con $AC$, allora $D$ è rimasto fuori dalla relazione.
-   tutti gli attributi senza archi entranti devono essere nella chiave
    candidata per forza, ma di fatto può succedere che non siano gli
    unici necessari.
-   $CDE$ allora può essere chiave candidata, perché riesce a derivare
    tutto.
-   $CDA$ è sempre chiave candidata.
-   $CDB$ è sempre chiave candidata

Sapute queste informazioni allora è facilissimo determinare se lo schema
è in BCMF, infatti basta controllare le relazioni nella definizione.
$F = \{A \to B, BC \to E, ED \to A\}$: tutte e tre le dipendenza
funzionali sono indice di ridondanza siccome non hanno chiave candidata.

La dipendenza funzionale $D \to B$ sussiste nello schema.

$D \to C$ sussiste pure?

Con sussiste si indica che il è parte delle cose sempre valide, già
identificate. La dipendenza funzionale di questo tipo è necessaria per
?? MANCA

$<R(A, B, C, D, E), F>$ e ci dice che $R_1(A, B, C)$ e $R_2(A, D, E)$ e
afferma "$A$ è chiave di $R_2$".

Questa relazione è senza perdita di informazione? $\forall$ istanza $r$
di $R$ che soddisfa $F$: $$
r = \pi_{ABC}(r) \Join \pi_{ADG}(r)
$$ In questo modo si ripristina la tabella di prima. Questa proprietà
però deve essere verificare per tutta la base di dati. Allora come
faccio a verificare che sia senza perdita di dati. C'è una modello
equivalente per verificarlo e è:

-   si prendono gli attributi in comune $R_1(\alpha _1)$,
    $R_2(\alpha _2)$ se $\alpha_1 \cap \alpha_2$ sono superchiavi di
    $R_1$ o $R_2$ allora la decomposizione è senza perdita di
    informazione.

Vogliamo trovare la **copertura minimale di $F$** che di solito
indichiamo come $F_{min}$.

1.  $F ^+ = F^+ _{min}$: queste rappresentazioni sono uguali
2.  $\forall x \to y \in F_{min}$ è tale che $y$ è un singolo attributo
3.  Le parti sinistre delle dipendenze funzionali di $F_{min}$ sono
    *minime*, ovvero se ad esempio $ABC \to D \in F_{min}$, se toglo
    anche un solo attributo da $ABC$ allore $F^+ \neq F^+_{min}$.
4.  $F_{min}$ non contiene dipendenze funzionali ridondanti. Se
    $x \to y \in F_{min}$, $(F_{min} - \{x \to y\})^+ \neq F^+$

Esempio:

$<R(A, B, C, D, E), F>$,
$F : A\to BC, CB \to E, D \to D, DB \to A, E \to C$:

1.  **normalizzazione**: se le dipendenze hanno più di un attributo
    sulla destra le spezzo in attributi con singoli elementi sulla
    destra:

    $A \to B, A \to C$, se trovi due tuple daccordo su $A$ allora devono
    essere daccordo anche su $C$

    \[Spezzare la parte di destra\]

2.  **riduzione delle parti sinistre**, gardantisce la terza proprietà:
    se togliamo anche un solo attributo da quella parte. $$
    A \to B \\
    A \to C \\
    B \to D \\
    E \to C 
    $$ Devo mettermi nella condizione in cui una determinazione di
    questo tipo non è utile. Devo dimostrare la chiusura di $C$: $$
    (C)^+ = C \\ 
    $$ MANCA

3.  Voglio sapere se $A\to B$ è ridondante. Voglio sapere se partendo da
    $A$ posso determinare $B$ ma senza determinare $E$. Di base quindi
    voglio sapere se eliminando $E$, $A$ continua a derivare.

    $(A)^+ = AC$, $A\to B$. $(A)^+ = A, A\to C$. Con questo valore

BCNF:

-   condizione triviale =\> se in condizione minimale allora queste
    condizioni non esistono
-   sono chiavi candidate o la contengono.

$E \to C$, dimostro che $(E)^+ = EC$ =\> dispongo di questa tabella in
questo modo.

Di fatto quindi la decomposizione si ottiene con due tabelle:

$R_1 (A, B, D, E,)$ e $R_2 (E, C)$.

A questi modelli aggiungo gli attributi a queste relazioni:

$<R_1(A, B, D, E), A\to B, B \to E, B \to D, B\to A$ e
$<R_2(E, C), E \to C$.

in questo modello tra l'altro sono riuscito a mantenere tutte le
dipendenze. Di fatto quindi si ottiene un modello completo, che permette
la corretta rappresentazione sul DBMS.
