# Install and role Checking PostgreSQL

```sh
sudo su postgres;
@postgres# psql;
psql@postgres# CREATE ROLE <name> LOGIN;
psql@postgres# ALTER ROLE <name> CREATEDB;
psql@postgres# ALTER ROLE <name> PASSWORD 'password';
```



## Implementazione Python

install dependency:

```python
pip3 install psycopg2-binary; # non binary version errored! If you'd like to try
```

Con un database già creato:

```sql
CREATE DATABASE <name>;
```

Poi inizio l’implementazione:

```python
import psycopg2

conn = psycopg2.connect("namedb=")
```

