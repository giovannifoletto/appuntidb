# Algebra Relazionale

Il DBMS ha un linguaggio di programmazione SQL che permette di interrogare il DBMS. Questo linguaggio intermedio che utilizza il DBMS per interpretare le operazioni richieste con i dati è chiamato **algebra relazionale**. 

Di fatto nei linguaggio dell’algebra relazionale permette di avere un ordine degli operatori. Nell’algebra relazionale si utilizza tra delle relazioni e si svolge attraverso operatori.

L’algebra relazionale utilizza un’insieme degli operatori di base:

* selezione
* proiezione
* prodotto in croce
* differenza
* unione

poi si aggiungono a questi degli operatori aggiuntivi che sono semplicemente dei wrapper intorno a più operatori di base.

Questo tipo di aritmetica è chiusa in queste operatori, ovvero che tutti gli elementi che elaboro con questi operatori rimane all’interno del campo dato in pasto in primo luogo.

Gli operatori di fatto poi si possono comporre per eseguire un’operazione complessa che esegue una serie di operazione a catena. Le operazioni non hanno un ordine di applicazione fisso, ma di fatto si cerca sempre di operare in modo più efficiente possibile.



## Operatore di Proiezione:

Questo operatore è indicato come $\pi$ e prende la tabella con i dati in input. Questo operatore ritorna le colonne della relazione.

Esempio: $\pi _{value, value2}(S)$. Ritorna due colonne con i valori $value1, value2$.

Il risultato di questo operatore è una nuova relazione, con tutti i dati organizzati in una nuova tabella, con le colonne prese con i valori indicati dall’operatore.

Questo operatore ha lo stesso vincolo sui duplicati che ha una relazione normalmente. Si ottiene quindi una relazione **senza duplicati** e **senza un ordine** preciso.

PostgreSQL non effettua il controllo sui duplicati per ottenere una funzione più efficiente, ma di fatto il modello matematico alla base di questa operazione fornisce una colonna di tuple univoche.



## Operatore di Selezione:

Questo operatore è di fatto la seconda direzione dell’operatore di proiezione. Questo operatore, indicato con $\sigma$, permette quindi di selezionare le righe che soddisfano delle condizioni.

Esempio: $\sigma _{value > num} (S)$.

Di fatto in questa operazione si possono utilizzare tutti gli operatori matematici disponibili.

In questa operazione non è possibile avere duplicati come risposta, siccome si parte da un istanza senza duplicati per definizione. Di fatto il numero di righe riportate è sempre di cardinalità minore o uguale rispetto al quella dell’istanza di partenza.



## Operatori Classici insiemistici:

Siccome l’algebra relazionale si applica a un insieme, allora si possono applicare a tutte le operazioni insiemistiche.

### Unione:

Questa operazione si può fare solo su tabelle compatibili, ovvero tutte le tabelle dell’operazione devono avere lo stesso numero di parametri e tutte le righe devono avere lo stesso dominio. Se non si ha una tabella con lo stesso dominio, oppure si vuole unire due tabelle che hanno un ordine delle colonne diverso, allora serve operare un ordinamento delle colonne.

L’operazione per ottenere due relazioni compatibili con due istanze di uguale cardinalità è ottenibile con l’operazione di proiezione ($\pi$).



### Sottrazione insiemistica

Questa operazione, dati due istanze $S$ e $T$ (con degli elementi), si svolge facendo il controllo di quello che appare su una delle due tabelle e rimuovere tutte le tuple dall’altra parte.



### Intersezione

L’intersezione di due insiemi ritorna gli elementi che due insiemi hanno in comune. Si ottiene di fatto con più operazioni elementari unite.

Esempio: $S \cap T = S - (S - T)$. 

Si noti come questa operazione è già un’operazione complessa dipendente da due operatori di base. Questo significato è che:

* prima si trova la differenza tra $S$ e $T$, ovvero l’intersezione tra i due insiemi.
* tolgo l’intersezione dall’insieme su cui voglio operare



## Prodotto Cartesiano:

Proveniente dalla teoria matematica e dalle operazioni tra insiemi. Questo operatore si può sempre applicare a due insiemi. Questo operatore è uno dei principali e più utilizzati nell’algebra relazionale.

Esempio: $S \times R$.  Queste due tabelle possono avere tutte cose sconnesse, senza nessun tipo di dato in comune potenzialmente. 

Questa operazione fa in modo che tutte le righe di una relazione vengano affiancata da tutte le righe dell’altra tabella, ovvero crea tutte le possibili combinazioni tra le righe di $S$ e $R$.

Se gli attributi hanno lo stesso nome non si può avere, si trova quindi un conflitto. L’algebra relazionale di solito rimuove questo nome della colonna e lo sostituisce con il valore della colonna (ad esempio il valore che si trova nella prima colonna, allora questo campo si chiamerà `$$1`).

Di fatto questa operazione è molto utilizzata accoppiandola con altre operazioni di base, infatti di solito si utilizza questo operatore per recuperare i dati di una seconda tabella collegata a una prima. Ad esempio: abbiamo due tabelle, una con `TABLE MARINAI(sid, sname, rating, age)`  e una seconda con `TABLE AFFITTO(sid, bid, day)` che indica gli affitti delle barche. Adesso vogliamo trovare il nome del marinaio che ha preso la barca (informazioni della seconda tabelle), ma in questa istanza è presente solo il `sid` del marinaio. Facendo il prodotto cartesiano si possono recuperare questi valori, collegando tutte le righe di marinaio con tutte le righe degli affitti. Si ottiene quindi una tabella del genere `TABLE F( [sid]$$1, sname, rating, age, [sid]$$2, bid, day)`. Facendo una selezione dove si cerca di avere `$$1` uguale a `$$2`, in modo che si ottengano i valori degli affitti uniti ai valori identificativi dei marinai.

Il problema di questo operatore è che è molto costoso, siccome deve fare combinazioni di due tabelle (ad esempio se $R$ ha $100000$ righe, $S$ ne ha altrettante, la tabella risultante ha cardinalità $10000000000$). Per questo motivo l’operatore di `join` (prodotto cartesiano più selezione) è si quello più usato, ma anche il più complicato dal punto di vista di operazioni per il calcolatore. Infatti i DBMS hanno una serie di modalità per semplificare questo operatore e svolgerlo nel modo più efficiente possibile.



### Operazione di renaming:

Quando si trovano due colonne con lo stesso nome, allora è necessario svolgere un operazione di *renaming*, indicato con $\rho$. 

Esempio: $\rho(C, R\times S)$, adesso ho a disposizione alla tabella risultante dall’operazione $R\times S$ sotto il nome di $C$. Inoltre in questo operatore posso anche indicare la modifica dei valori delle colonne attraverso questa operazione $\rho(C(1 \to sid1, 5 \to sid2), R\times S)$. La parte che rinomina le colonne della relazione risultante di $C$ non è fondamentale, si può lasciare tranquillamente rinominare automaticamente. 

Dopo questa operazione dispongo di una “tabella temporanea” chiamata $C$ che si può utilizzare in altre operazioni, tipo ad esempio la selezione $\sigma _{sid1=sid2} (C)$. Se da questo voglio ottenere solo i nomi di queste due operazioni, allora posso eseguire a cascata una proiezione $\pi_{sname} (\sigma _{sid1 = sid2} (C))$. 

Questo operatore si può utilizzare anche per rinominare dei singoli parametri di una tabella, indicando $\rho _{A \to B} (R)$ (questa operazione restituisce una tabella non nominata, ma con il parametro della tabella di partenza $A$ chiamato $B$).



## Joins

Questo operatore è un prodotto cartesiano con una selezione nello stesso momento. Questa operazione è così comune che si utilizza un operatore nuovo per eseguire questa funzione.

Ci sono più tipi di $join$ che in realtà sono tutte derivate e particolari della *condition join*.

### Condition Join:

Questa operazione si semplifica con: 
$$
S \Join_c R = \sigma_c(S \times R) 
$$
Di fatto questo operatore si occupa anche del rinominare le colonne che hanno un *clash* di nome.

Per evitare questi problemi si possono utilizzare le *dot notations*:
$$
S \Join _{S.sid < R.sid} R
$$
[questa operazione indica: prendi il prodotto cartesiano $S \times R$, poi prendere solo le righe in cui il parametro `id` di `S` è minore di quello di `R`].

Di fatto questa operazione, se avessimo dovuto fare tutte le operazioni in fila, avremmo spesso bisogno di operatori di *renaming*. Per questo motivo si utilizza questo modello per indicare che parametri si utilizzano.

Di fatto, essendo un operatore binario, so per certo che si creerà una relazione tra due istanze (e la *dot notation* diviene necessaria per evitare incomprensioni).



### Natural Join

Il primo caso speciale di *join* che si merita un nome a parte siccome particolarmente utilizzata è la **natural join**. Questa operazione è semplificabile in questo modo:
$$
S \Join R = \pi _{X, Y-X} (S\Join _{eq. on X \cap Y} R)
$$
La *join* naturale si scrive senza indicazioni, perché si da per scontato che si faccia l’operazione di confronto sugli attributi con nome uguale. 

Questo significa che se faccio la *join naturale* tra due istanze che hanno in comune un valore. 

Esempio: ho due tabelle `TABLE MARINAI(sid, name, rating, age)` e `TABLE AFFITTO(sid, bid, day)`. Allora l’operatore di *join naturale* tornerà una tabella con la join fatta su tutti gli elementi e restituisce una selezione di eguaglianza sul parametro `sid` in entrambe. Questo permette perfettamente di fare la ricerca voluta precedentemente.

Se le tabelle fossero state `TABLE MARINAI(sid, name, rating, bid)` e `TABLE AFFITTO(sid, bid, day)` allora si otterrà una tabella con gli elementi contenenti solo le righe con `sid` e `bid` uguali.

Di fatto questa tabella avrebbe sempre almeno due colonne con esattamente gli stessi valori, siccome la *join naturale* si ottiene sempre con operazioni di uguaglianza. Per questo motivo evita di duplicare le colonne con lo stesso valore, siccome tanto non servirebbe riportarlo. Questa operazione non avviene nella *join condizionale*, che non per forza impone la presenza di colonne sempre uguali.



## Operatore di Divisione

Questo operatore non primitivo e accetta solo due valori in input. 

Si opera in questo modo tra due tabelle e restituisce una tabella che ha gli elementi di tutte le tuple della seconda tabella.

?

Questa tabella si spezza ottenendo il valore che hanno in comune.



## Esempio di Algebra Relazionale

```sql
Voli(codV: int, origin: string, dest: string, distance: int, ora_part: time, ora_arrivo: time);
Aerei(codA: int, nameA: string, dist_perc: int);
Certificati(codD: int, codA: int, FK(codD)R(Dipendenti.codD), FK(codA)R(Aerei.codA));
Dipendenti(codD: int, nameD: string, stipendio: int);
```

1. Piloti certificati per qualche aereo
   $$
   \pi _{codD} (Certificati) \Join Dipendenti \\
   \rho (DipCert, \pi_{codD}(Certificati)) \\
   DipCert \Join_{DipCert.codD = Dipendenti.codD} Dipententi \\
   $$
   Un altro modo per fare questa ricerca è:
   $$
   \pi_{nameD}(Certificati \Join Dipendenti) \\
   $$
   L’unica differenza tra queste due interrogazioni è che con la *join naturale* non accede alle colonne uguali, siccome le rimuove. La *join condizionale* invece subisce per forza un clash di nomi e quindi ha necessità di rinominare le colonne.

2. Aerei che possono volare non-stop da ‘Bonn’ a ‘Madrid’
   $$
   \rho(DistBM, \pi(\sigma_{orgin: `Bonn` < dist: `Madrid`}(Voli))) \\
   \pi _{codA} (Aerei \Join DistBM)
   $$

3. Voli effettuabili da qualche pilota con stipendio maggiore di $100000$$

   1. Cerco le combinazioni Aereo e Voli
      $$
      \rho (VoloAereo,\pi_{codV, codA} (Voli \Join_{distanza \le dist_perc} Aerei) )
      $$

   2. Cerco i dipendenti con stipendio maggiore di $100000$$.
      $$
      \pi_{codA}(\pi_{codD}(\sigma_{stipendio > 100000} (Dipendenti)) \Join Certificati)
      $$
      => risultato una tabella con `codA` e `codB` e ci sono solo i piloti con i certificati degli aerei pilotabili con piloti con alto stipendio

   3. Adesso interseco 2 relazioni sopra per ottenere i piloti che possono usare aerei per volo di grande distanza con un grande stipendio
      $$
      \pi_{codV} ( VoloAereo \Join \pi_{codA}(\pi_{codD}(\sigma_{stipendio > 100000} (Dipendenti)) \Join Certificati))
      $$
      => ottengo soltanto i codici dei voli come da richiesta

   4. Un altro modo per fare questa operazione è accorgersi che questa operazione è ottenibile con 4 join e poi un filtraggio:
      $$
      \pi_{codV}(Voli \Join_{dist \le dist\_perc } \sigma_{stipendio > 100000}(Aerei \Join Certificati \Join Dipendenti))
      $$
      Allora, le operazioni fatte in questo comando sono:

      - $Aerei \Join Certificati$ permette di far corrispondere il `codA`
      - $Certificati \Join Dipendenti$ permette di far corrispondere i `codD`
      - si crea una tabella con `aerei`, `certificati` e `dipendenti`  tutti connessi tra di loro in modo che poi ho `aereo, pilota_corrisp_aereo, stipendio`
      - Si filtra per lo stipendio maggiore di $100000$$
      - Di fatto un operazione di questo modello in un DBMS verrebbe sempre evitato, siccome le operazioni di $\Join$ sono le più computazionalmente difficili.

Esempio 2:

```sql
Attori(codA:int, nome:string, annoN:int, naz:string, KEY(codA));
Film(codF:int, titolo:string, durata:int, regista: int, KEY(codF));
Registi(codR: int, nome:string, annoN:int, KEY(codR), FK(regista)R(Registi));
Recita(attore: int, film:int, ruolo:string, K(attore, film), FK(attore)R(Attori), FR(film)R(Film))
```

1. Nomi degli attori nati dopo il 1950
   $$
   \pi_{nome}(\sigma_{annoN>1950}(Attori))
   $$
   Da notare che questa interrogazione restituisce i nomi degli attori univoci, quindi di fatto se ci sono due nomi uguali di attori diversi, allora viene mostrato una volta sola. Di fatto per rappresentare tutti gli attori univocamente è necessario richiedere il `codA` degli attori.

2. Nomi degli attori che hanno recitato in almeno un film:
   $$
   \pi_{nome}(Attori \Join_{attori=codA} Recita)
   $$

3. Nomi degli attori che hanno recitato almeno in due film:
   $$
   \rho(R1, Recita) \\ 
   \rho(R2, Recita) \\ 
   (\pi_{R1.attore}(R1 \Join_{R1.attore = R2.attore\space \& \space R1.film \neq R2.film} R2) ) \Join_{attore=codA} Attori\\
   $$
   Questa soluzione permette di fare una $\Join$ sulla stessa tabella. Poi controllo che il valore dell’attore sia sullo stesso film un unica volta. Poi unisco la tabella trovata con la tabella degli attori.

4. Adesso bisogna trovare i nomi degli attori che hanno recitato in esattamente un film.
   $$
   \rho(Almeno1, \pi_{attore}(Recita)) \\
   \rho(Almeno2, \pi_{R1.attore}(R1 \Join_{R1.attore = R2.attore \space \& \space R1.film \neq R2.film} R2)) \\
   \pi_{nome}((Almeno1 - Almeno2) \Join_{attore=codA} Attori)
   $$

5. Attori che non hanno interpretato qualche ruolo.

   (cerchiamo di fare questa interrogazione senza divisione, che richiede tantissimi operatori)
   $$
   \pi_{nome}((\pi_{codA}(Attore) \times \pi_{ruolo}(Recita)) - \pi_{attore, recita}(Recita))
   $$
   [Con questa interrogazione si ottiene un valore con delle coppie false di attori e ruoli, ovvero degli attori che non hanno il ruolo fatto. Da questa relazione si sottraggono le coppie vere]

6. Attori che hanno recitato in tutti i ruoli
   $$
   (\pi_{attori, ruolo}(Recita) / \pi_{roulo}(Recita)) \Join_{Recita1.attore = Recita2.attore \space \& \space Recita1.attore = Recita2.attore} Recita
   $$



